#pragma once
#include <cstdint>

namespace int_gemm {

    struct uint64x2_t {

        uint64_t lo;
        uint64_t hi;

        __host__ __device__
        uint64x2_t();

        __host__ __device__
        uint64x2_t(uint64_t hi, uint64_t lo);

        __host__ __device__
        explicit uint64x2_t(__uint128_t value);

        __uint128_t to_uint128() const;

        inline explicit operator __uint128_t() const {
            return this->to_uint128();
        }

        inline operator bool() const {
            return (this->hi != 0) || (this->lo != 0);
        }

        __host__ __device__ 
        uint64x2_t operator +(const uint64x2_t& rhs) const;

        inline __host__ __device__
        uint64x2_t& operator +=(const uint64x2_t& rhs) {
            *this = *this + rhs;
            return *this;
        }

        __host__ __device__
        uint64x2_t operator -(const uint64x2_t& rhs) const;

        __host__ __device__
        uint64x2_t operator *(const uint64x2_t& rhs) const;

        inline __host__ __device__
        uint64x2_t& operator *=(const uint64x2_t& rhs) {
            *this = *this * rhs;
            return *this;
        }
        
        inline __host__ __device__ 
        bool operator ==(const uint64x2_t& rhs) const {
            return (this->hi == rhs.hi) && (this->lo == rhs.lo);
        }

        inline __host__ __device__
        bool operator !=(const uint64x2_t& rhs) const {
            return (this->hi != rhs.hi) || (this->lo != rhs.lo);
        }


    };

    void deviceUint64x2Matmul(
        size_t M, size_t N, size_t K,
        const uint64x2_t *A, const uint64x2_t *B, uint64x2_t *C
    );

}