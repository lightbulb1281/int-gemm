#pragma once
#include <cstdint>
#include <cstddef>

namespace int_gemm {

    const size_t  INT32_CONSECUTIVE_THRESHOLD = 10000;
    const size_t  INT64_CONSECUTIVE_THRESHOLD = 100000;
    const size_t INT128_CONSECUTIVE_THRESHOLD = 100000;
    const size_t  INT32_STRIDED_THRESHOLD = 10000;
    const size_t  INT64_STRIDED_THRESHOLD = 50000;
    const size_t INT128_STRIDED_THRESHOLD = 100000;

    // int32 implementation
    
    void deviceInt32Matmul(
        size_t M, size_t N, size_t K,
        const int32_t *A, const int32_t *B, int32_t *C
    );

    void hostInt32MatmulStrided(
        size_t M, size_t N, size_t K,
        const int32_t* A, const int32_t* B, int32_t* C,
        size_t strideA, size_t strideB, size_t strideC
    );

    void hostInt32Matmul(
        size_t M, size_t N, size_t K,
        const int32_t* A, const int32_t* B, int32_t* C
    );

    inline void deviceUint32Matmul(
        size_t M, size_t N, size_t K,
        const uint32_t *A, const uint32_t *B, uint32_t *C
    ) {
        deviceInt32Matmul(M, N, K, (const int32_t*)(A), (const int32_t*)(B), (int32_t*)(C));
    }
    
    inline void hostUint32MatmulStrided(
        size_t M, size_t N, size_t K,
        const uint32_t* A, const uint32_t* B, uint32_t* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {
        hostInt32MatmulStrided(M, N, K, (const int32_t*)(A), (const int32_t*)(B), (int32_t*)(C), strideA, strideB, strideC);
    }

    inline void hostUint32Matmul(
        size_t M, size_t N, size_t K,
        const uint32_t* A, const uint32_t* B, uint32_t* C
    ) {
        hostInt32Matmul(M, N, K, (const int32_t*)(A), (const int32_t*)(B), (int32_t*)(C));
    }

    // int64 implementation

    void deviceInt64Matmul(
        size_t M, size_t N, size_t K,
        const int64_t *A, const int64_t *B, int64_t *C
    );

    void hostInt64MatmulStrided(
        size_t M, size_t N, size_t K,
        const int64_t* A, const int64_t* B, int64_t* C,
        size_t strideA, size_t strideB, size_t strideC
    );

    void hostInt64Matmul(
        size_t M, size_t N, size_t K,
        const int64_t* A, const int64_t* B, int64_t* C
    );

    inline void deviceUint64Matmul(
        size_t M, size_t N, size_t K,
        const uint64_t *A, const uint64_t *B, uint64_t *C
    ) {
        deviceInt64Matmul(M, N, K, (const int64_t*)(A), (const int64_t*)(B), (int64_t*)(C));
    }
    
    inline void hostUint64MatmulStrided(
        size_t M, size_t N, size_t K,
        const uint64_t* A, const uint64_t* B, uint64_t* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {
        hostInt64MatmulStrided(M, N, K, (const int64_t*)(A), (const int64_t*)(B), (int64_t*)(C), strideA, strideB, strideC);
    }

    inline void hostUint64Matmul(
        size_t M, size_t N, size_t K,
        const uint64_t* A, const uint64_t* B, uint64_t* C
    ) {
        hostInt64Matmul(M, N, K, (const int64_t*)(A), (const int64_t*)(B), (int64_t*)(C));
    }

    // int128 implementation

    void hostInt128MatmulStrided(
        size_t M, size_t N, size_t K,
        const __int128_t* A, const __int128_t* B, __int128_t* C,
        size_t strideA, size_t strideB, size_t strideC
    );

    void hostInt128Matmul(
        size_t M, size_t N, size_t K,
        const __int128_t* A, const __int128_t* B, __int128_t* C
    );
    
    inline void hostUint128MatmulStrided(
        size_t M, size_t N, size_t K,
        const __uint128_t* A, const __uint128_t* B, __uint128_t* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {
        hostInt128MatmulStrided(M, N, K, (const __int128_t*)(A), (const __int128_t*)(B), (__int128_t*)(C), strideA, strideB, strideC);
    }

    inline void hostUint128Matmul(
        size_t M, size_t N, size_t K,
        const __uint128_t* A, const __uint128_t* B, __uint128_t* C
    ) {
        hostInt128Matmul(M, N, K, (const __int128_t*)(A), (const __int128_t*)(B), (__int128_t*)(C));
    }

    // int32 as double * 2 implementation
    
    void hostUint32AsDoubleMatmulStrided(
        size_t M, size_t N, size_t K,
        const uint32_t* A, const uint32_t* B, uint32_t* C,
        size_t strideA, size_t strideB, size_t strideC
    );

    // int64 as double * 4 implementation
    
    void hostUint64AsDoubleMatmulStrided(
        size_t M, size_t N, size_t K,
        const uint64_t* A, const uint64_t* B, uint64_t* C,
        size_t strideA, size_t strideB, size_t strideC
    );

    // int128 as double * 8 implementation
    
    void hostUint128AsDoubleMatmulStrided(
        size_t M, size_t N, size_t K,
        const __uint128_t* A, const __uint128_t* B, __uint128_t* C,
        size_t strideA, size_t strideB, size_t strideC
    );


}