
#pragma once
#include <map>
#include <stdexcept>
#include <mutex>

namespace int_gemm {
    
    class MemoryPool {

        const size_t preservedMemory = 1024 * 1024 * 32;

    private:
        static MemoryPool singleton;
        std::mutex mutex;

        MemoryPool() {
            cudaSetDevice(0);
            allocated = 0;
            // cudaDeviceProp props; cudaGetDeviceProperties(&props, 0);
            // size_t totalMemory = props.totalGlobalMem;
            // printf("[MemoryPoolCuda] Initialize, Total Memory = %ld bytes\n", totalMemory);
        }

        std::multimap<size_t, void*> pointers;
        size_t allocated;
        // size_t totalMemory;

        inline size_t release() {
            if (pointers.size() == 0) return 0;
            size_t released = 0;
            for (auto& pair: pointers) {
                // printf("free address = %p\n", pair.second);
                cudaError_t result = cudaFree(pair.second);
                if (result != cudaSuccess) {
                    throw std::runtime_error("Failed to free array");
                }
                released += pair.first;
            }
            // printf("[MemoryPoolCuda] Released %ld bytes\n", released);
            allocated -= released;
            pointers.clear();
            return released;
        }

        inline void* tryAllocate(size_t require) {
            size_t free, total;
            cudaMemGetInfo(&free, &total);
            if (free < require + preservedMemory) release();
            allocated += require;
            void* array;
            cudaError_t result = cudaMalloc(&array, require);
            // printf("[MemoryPoolCuda] Try allocate %ld bytes, result code = %d\n", require, result);
            if (result != cudaSuccess) {
                // write message to a string
                char buffer[1024];
                snprintf(buffer, 1024, "Failed to allocate array of size %ld bytes, %ld bytes free, %ld bytes allocated. Malloc result code = %d", require, free, allocated, result);
                // throw exception
                throw std::runtime_error(buffer);
            }
            // printf("[MemoryPoolCuda] Allocate success.\n");
            return array;
        }

        inline void* get(size_t require) {
            
            // void* array;
            // cudaError_t result = cudaMalloc(&array, require);
            // if (result != cudaSuccess) {
            //     throw std::runtime_error("[MemoryPoolCuda] Failed to allocate.");
            // }
            // return array;

            std::lock_guard<std::mutex> lock(mutex);
            // printf("[MemoryPoolCuda] Mutex locked\n");
            auto iterator = pointers.lower_bound(require);
            if ((iterator == pointers.end()) || (iterator->first > require * 2)) {
                void* ret = tryAllocate(require);
                // printf("[MemoryPoolCuda] Allocated. Mutex released.\n");
                return ret;
            } else {
                void* p = iterator->second;
                pointers.erase(iterator);
                // printf("[MemoryPoolCuda] Found idle. Mutex released.\n");
                return p;
            }
        }

        void insert(void* ptr, size_t size) {

            // cudaFree(ptr);
            // return;

            std::lock_guard<std::mutex> lock(mutex);
            // printf("returned address = %p\n", ptr);
            pointers.insert(std::make_pair(size, ptr));
        }

        ~MemoryPool() {
            release();
        }

    public:

        inline static void* Get(size_t require) {
            return singleton.get(require);
        }

        inline static void Free(void* ptr, size_t size) {
            singleton.insert(ptr, size);
        }

        inline static void Release() {
            singleton.release();
        }

    };
}