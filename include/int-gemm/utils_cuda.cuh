#pragma once
#include <stdint.h>
#include <stdexcept>
#include <iostream>
#include "memorypool.cuh"

namespace int_gemm {

    const size_t CUDA_STREAMS_COUNT = 4;

    cudaStream_t* getCudaStreams();
    
    inline bool cudaAvailable() {
        int deviceCount;
        cudaError_t result = cudaGetDeviceCount(&deviceCount);
        return result == cudaSuccess && deviceCount > 0;
    }

    template<typename T>
    T* allocateDeviceArray(size_t size) {
        // T *array;
        // cudaError_t result = cudaMalloc(reinterpret_cast<void**>(&array), size * sizeof(T));
        // if (result != cudaSuccess) {
        //     throw std::runtime_error("Failed to allocate array");
        // }
        // return array;

        T *array = reinterpret_cast<T*>(MemoryPool::Get(size * sizeof(T)));
        return array;
    }

    template<typename T>
    void freeDeviceArray(T *array, size_t size) {
        // cudaError_t result = cudaFree(array);
        // if (result != cudaSuccess) {
        //     throw std::runtime_error("Failed to free array");
        // }
        MemoryPool::Free(reinterpret_cast<void*>(array), size * sizeof(T));
    }

    template<typename T>
    void copyToDeviceStrided(T *dest, const T *src, size_t stride, size_t count) {
        if (stride == 1) {
            cudaError_t result = cudaMemcpy(
                dest, src, count * sizeof(T), cudaMemcpyHostToDevice);
            if (result != cudaSuccess) {
                throw std::runtime_error("Failed to copy to device");
            }
            return;
        }
        cudaError_t result = cudaMemcpy2D(
            dest, sizeof(T), src, stride * sizeof(T), 
            sizeof(T), count, cudaMemcpyHostToDevice);
        if (result != cudaSuccess) {
            throw std::runtime_error("Failed to copy to device");
        }
    }

    template<typename T> 
    void copyToDevice(T *dest, const T *src, size_t size) {
        copyToDeviceStrided(dest, src, 1, size);
    }

#ifdef INT_GEMM_COPYTOHOST_CUDASTREAMS

    template<typename T>
    void copyToHostStrided(T *dest, const T *src, size_t stride, size_t size) {
        cudaStream_t* streams = getCudaStreams();
        cudaError_t result;
        size_t interval = size / CUDA_STREAMS_COUNT;
        for (size_t s = 0; s < CUDA_STREAMS_COUNT; s++) {
            size_t lower = s * interval;
            size_t count = (s == CUDA_STREAMS_COUNT - 1) ? (size - lower) : interval;
            if (stride == 1) {
                result = cudaMemcpyAsync(
                    dest + lower, 
                    src + lower, 
                    count * sizeof(T), cudaMemcpyDeviceToHost, streams[s]
                );
            } else {
                result = cudaMemcpy2DAsync(
                    dest + lower * stride, stride * sizeof(T), 
                    src + lower, sizeof(T), 
                    sizeof(T), count, cudaMemcpyDeviceToHost, streams[s]
                );
            }
            if (result != cudaSuccess) {
                printf("Failed to copy to host: %s\n", cudaGetErrorString(result));
                throw std::runtime_error("Failed to copy to host");
            }
        }
        cudaDeviceSynchronize();
    }

#else

    template<typename T>
    void copyToHostStrided(T *dest, const T *src, size_t stride, size_t count) {
        if (stride == 1) {
            cudaError_t result = cudaMemcpy(dest, src, count * sizeof(T), cudaMemcpyDeviceToHost);
            if (result != cudaSuccess) {
                throw std::runtime_error("Failed to copy to host");
            }
            return;
        }
        cudaError_t result = cudaMemcpy2D(dest, stride * sizeof(T), src, sizeof(T), sizeof(T), count, cudaMemcpyDeviceToHost);
        if (result != cudaSuccess) {
            throw std::runtime_error("Failed to copy to host");
        }
    }

#endif


    template<typename T> 
    void copyToHost(T *dest, const T *src, size_t size) {
        copyToHostStrided(dest, src, 1, size);
    }

    // The inputs A, B and C should already be on the GPU memory
    // and they are in column-major format.
    void deviceDoubleMatmul(
        size_t M, size_t N, size_t K,
        const double *A, const double *B, double *C
    );

    void deviceArrayAddInplace(
        size_t size,
        double *A, const double *B
    );

    // A += (int)(B / (1<<16)); B = B % (1<<16)
    void deviceArrayCarry16(
        size_t size,
        double *A, double *B
    );

    // A = A % (1<<16)
    void deviceArrayReduce16Inplace(
        size_t size, double *A
    );

    inline void debugPrintDeviceArray(const double *array, size_t size) {
        double *hostArray = new double[size];
        copyToHost(hostArray, array, size * sizeof(double));
        std::cout << "[";
        for (size_t i = 0; i < size; i++) {
            printf("%.0lf", hostArray[i]);
            if (i != size - 1) {
                std::cout << ", ";
            }
        }
        std::cout << "]";
        std::cout << std::endl;
        delete[] hostArray;
    }

    template <typename Integer>
    void cpuIntMatmulStrided( 
        const size_t M,
        const size_t N,
        const size_t K,
        const Integer *A,
        const Integer *B,
        Integer *C,
        size_t strideA,
        size_t strideB,
        size_t strideC
    ) {
        for (size_t m = 0; m < M; ++m) {
            for (size_t n = 0; n < N; ++n) {
                Integer sum = Integer(0);
                for (size_t k = 0; k < K; ++k) {
                    sum += A[(m * K + k) * strideA] * B[(k * N + n) * strideB];
                }
                C[(m * N + n) * strideC] = sum;
            }
        }
    }

}