from performance_test import run_performance_test

def is_gpu_better(bitwidth: str, m, n, k, stride=1):
    result = run_performance_test(bitwidth, m, n, k, stride, stride, 1)
    ret = result["CudaTotal"] < result["ReferenceMatmul"]
    print("GPU better at ('{5}', {0}, {1}, {2}, {3}) == {4}".format(m, n, k, stride, ret, bitwidth))
    return ret

def find_boundary(bitwidth:str, m, n, start_k=None, stride=None):

    # at start, (m, n, k) should form a CPU advantage
    # but at larger k, the GPU parallel will be faster
    # this function finds the k where the boundary is

    if start_k is None:
        start_k = max(m, n)
    k = start_k
    while not is_gpu_better(bitwidth, m, n, k, stride):
        k *= 2
    
    # now we know that (m, n, k) is a GPU advantage
    # we can binary search to find the boundary
    left = k // 2
    right = k
    while left + 1 < right:
        mid = (left + right) // 2
        if not is_gpu_better(bitwidth, m, n, mid, stride):
            left = mid
        else:
            right = mid
    
    return left

print(find_boundary("32", 50, 50, 50, 1))
print(find_boundary("32", 50, 100, 20, 1))
print(find_boundary("32", 50, 200, 5, 1))
print(find_boundary("32", 100, 200, 1, 1))
#10000 

print(find_boundary("64", 50, 50, 50, 1))
print(find_boundary("64", 50, 100, 20, 1))
print(find_boundary("64", 50, 200, 5, 1))
print(find_boundary("64", 100, 200, 1, 1))
#100000

print(find_boundary("128", 50, 50, 50, 1))
print(find_boundary("128", 50, 100, 20, 1))
print(find_boundary("128", 50, 200, 5, 1))
print(find_boundary("128", 100, 200, 1, 1))
#100000

    
print(find_boundary("32", 50, 50, 50, 2))
print(find_boundary("32", 50, 100, 20, 2))
print(find_boundary("32", 50, 200, 5, 2))
print(find_boundary("32", 100, 200, 1, 2))
#10000 

print(find_boundary("64", 50, 50, 50, 2))
print(find_boundary("64", 50, 100, 20, 2))
print(find_boundary("64", 50, 200, 5, 2))
print(find_boundary("64", 100, 200, 1, 2))
#50000

print(find_boundary("128", 50, 50, 50, 2))
print(find_boundary("128", 50, 100, 20, 2))
print(find_boundary("128", 50, 200, 5, 2))
print(find_boundary("128", 100, 200, 1, 2))
#100000