import subprocess

executable = "../build/test/int_gemm_performance"
watched_outputs = [
    "Allocate",
    "CopyToDevice",
    "Decompose",
    "CudaMatmul",
    "Compose",
    "CopyToHost",
    "Free",
    "ReferenceMatmul",
]
columns = [
    "Bitwidth",
    "M",
    "N",
    "K",
    "StrideA",
    "StrideB",
    "StrideC",
    "Allocate",
    "CopyToDevice",
    "Decompose",
    "CudaMatmul",
    "Compose",
    "CopyToHost",
    "Free",
    "CudaTotal",
    "ReferenceMatmul",
    "Speedup",
]

def run_performance_test(bitwidth_string, m, n, k, stride_a, stride_b, stride_c):

    args = [executable, bitwidth_string, str(m), str(n), str(k), str(stride_a), str(stride_b), str(stride_c)]
    args_str = " ".join(args)
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    stdout = stdout.decode("utf-8")
    stderr = stderr.decode("utf-8")
    if p.returncode != 0:
        raise RuntimeError("Error running performance test: " + args_str)
        
    # split stdout by lines
    lines = stdout.split("\n")

    results = {}
    # set all to 0
    for watched_output in watched_outputs:
        results[watched_output] = 0.0

    # for all watched outputs, examine "[name] xx us"
    for line in lines:
        for watched_output in watched_outputs:
            if line.startswith(watched_output):
                # split by space
                tokens = line.split(" ")
                # last token is the time
                time = float(tokens[1])
                results[watched_output] = time
    
    # cuda total is all except "ReferenceMatmul"
    results["CudaTotal"] = sum([results[x] for x in watched_outputs if x != "ReferenceMatmul"])

    return results

def print_to_file(file_descriptor, result_dict):
    # print as csv, columns ranged as in "columns"
    for column in columns:
        file_descriptor.write(str(result_dict[column]) + ", ")
    file_descriptor.write("\n")

def print_headers(file_descriptor):
    for column in columns:
        file_descriptor.write(column + ", ")
    file_descriptor.write("\n")

if __name__ == "__main__":

    bitwidth_strings = [
        "32", "64", "128",
        "32d", "64d", "128d",
    ]

    settings = [
        ( 10,  10,  10, 1, 1, 1),
        (500,  10,  10, 1, 1, 1),
        ( 10, 500,  10, 1, 1, 1),
        ( 10,  10, 500, 1, 1, 1),
        (500, 500,  10, 1, 1, 1),
        (500,  10, 500, 1, 1, 1),
        ( 10, 500, 500, 1, 1, 1),
        (500, 500, 500, 1, 1, 1),
        (500, 500, 500, 2, 2, 1),
        (500, 500, 500, 4, 4, 1),
    ]

    with open("../build/performance_test.csv", "w") as f:
        print_headers(f)
        for bitwidth_string in bitwidth_strings:
            print("Running performance test for bitwidth " + bitwidth_string)
            for setting in settings:
                m, n, k, stride_a, stride_b, stride_c = setting
                print("Running performance test for setting " + str(setting))
                result_dict = run_performance_test(bitwidth_string, m, n, k, stride_a, stride_b, stride_c)
                result_dict["Bitwidth"] = bitwidth_string
                result_dict["M"] = m
                result_dict["N"] = n
                result_dict["K"] = k
                result_dict["StrideA"] = stride_a
                result_dict["StrideB"] = stride_b
                result_dict["StrideC"] = stride_c
                result_dict["Speedup"] = result_dict["ReferenceMatmul"] / result_dict["CudaTotal"]
                print("  Acceleration: " + str(result_dict["Speedup"]))
                print_to_file(f, result_dict)
    