cd ..

cd build
cmake .. -DINT_GEMM_COPYTOHOST_CUDASTREAMS=ON
make int_gemm_performance
cd ../scripts
python3 performance_test.py
cd ..
mv build/performance_test.csv build/result_cudastreams.csv

cd build
cmake .. -DINT_GEMM_COPYTOHOST_CUDASTREAMS=OFF
make int_gemm_performance
cd ../scripts
python3 performance_test.py
cd ..
mv build/performance_test.csv build/result_nocudastreams.csv