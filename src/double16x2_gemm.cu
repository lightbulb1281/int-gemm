#include "include/int-gemm/int_gemm.h"
#include "include/int-gemm/utils_cuda.cuh"

#define KERNEL_CALL(funcname, n) funcname<<<((n) + 255) / 256, 256>>>
#define GLOBAL_INDEX (blockDim.x * blockIdx.x + threadIdx.x)

namespace int_gemm {

    using Integer = uint32_t;

    // Put the lower 16 bits of intBuffer[i] to doubleBuffer[i] and higher 16 bits to doubleBuffer[i + n] 
    __global__ void decomposeUint32(double* doubleBuffer, uint32_t* intBuffer, size_t n) {
        size_t globalIndex = GLOBAL_INDEX;
        if (globalIndex < n) {
            uint32_t value = intBuffer[globalIndex];
            doubleBuffer[globalIndex] = (double)(value & 0xFFFF);
            doubleBuffer[globalIndex + n] = (double)(value >> 16);
        }
    }

    // Inverse of decomposeUint32
    __global__ void composeUint32(uint32_t* intBuffer, double* doubleBuffer, size_t n) {
        size_t globalIndex = GLOBAL_INDEX;
        if (globalIndex < n) {
            uint32_t value = 
                ((uint32_t)(round(doubleBuffer[globalIndex]))) | 
                ((uint32_t)(round(doubleBuffer[globalIndex + n])) << 16);
            intBuffer[globalIndex] = value;
        }
    }

    void hostUint32AsDoubleMatmulStrided(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {

        // create device memory
        size_t sizeA = M * K;
        size_t sizeB = K * N;
        size_t sizeC = M * N;
        size_t total = sizeA + sizeB + sizeC;
        Integer* intBuffer = allocateDeviceArray<uint32_t>(total);
        double* doubleBuffer = allocateDeviceArray<double>(total * 2 + sizeC);
        Integer* intA = intBuffer;
        Integer* intB = intBuffer + sizeA;
        Integer* intC = intBuffer + sizeA + sizeB;
        double* doubleA = doubleBuffer;
        double* doubleB = doubleBuffer + sizeA * 2;
        double* doubleC = doubleBuffer + (sizeA + sizeB) * 2;
        double* temp = doubleBuffer + total * 2;

        // copy to device memory
        copyToDeviceStrided(intA, A, strideA, sizeA);
        copyToDeviceStrided(intB, B, strideB, sizeB);

        // decompose into double
        KERNEL_CALL(decomposeUint32, sizeA)(doubleA, intA, sizeA);
        KERNEL_CALL(decomposeUint32, sizeB)(doubleB, intB, sizeB);

        // matrix multiplication
        // - lower 16 bits
        deviceDoubleMatmul(N, M, K, doubleB, doubleA, doubleC);
        // - higher 16 bits
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA, doubleC + sizeC);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, doubleC + sizeC, temp);
        // - carry
        deviceArrayCarry16(sizeC, doubleC + sizeC, doubleC);
        // - overflow
        deviceArrayReduce16Inplace(sizeC, doubleC + sizeC);

        // compose back to int
        KERNEL_CALL(composeUint32, sizeC)(intC, doubleC, sizeC);

        // copy back to host memory
        copyToHostStrided(C, intC, strideC, sizeC);

        // free device memory
        freeDeviceArray(intBuffer, total);
        freeDeviceArray(doubleBuffer, total * 2 + sizeC);
    }    

}