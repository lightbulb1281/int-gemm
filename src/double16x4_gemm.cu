#include "include/int-gemm/int_gemm.h"
#include "include/int-gemm/utils_cuda.cuh"

#define KERNEL_CALL(funcname, n) funcname<<<((n) + 255) / 256, 256>>>
#define GLOBAL_INDEX (blockDim.x * blockIdx.x + threadIdx.x)
namespace int_gemm {

    using Integer = uint64_t;

    // 0~16 bit of intBuffer[i] to doubleBuffer[i]
    // 16~32 bit of intBuffer[i] to doubleBuffer[i + n]
    // 32~48 bit of intBuffer[i] to doubleBuffer[i + 2n]
    // 48~64 bit of intBuffer[i] to doubleBuffer[i + 3n]
    __global__ void decomposeUint64(double* doubleBuffer, uint64_t* intBuffer, size_t n) {
        size_t globalIndex = GLOBAL_INDEX;
        if (globalIndex < n) {
            uint64_t value = intBuffer[globalIndex];
            doubleBuffer[globalIndex] = (double)(value & 0xFFFF);
            doubleBuffer[globalIndex + n] = (double)((value >> 16) & 0xFFFF);
            doubleBuffer[globalIndex + n * 2] = (double)((value >> 32) & 0xFFFF);
            doubleBuffer[globalIndex + n * 3] = (double)((value >> 48) & 0xFFFF);
        }
    }

    // Inverse of decomposeUint64
    __global__ void composeUint64(uint64_t* intBuffer, double* doubleBuffer, size_t n) {
        size_t globalIndex = GLOBAL_INDEX;
        if (globalIndex < n) {
            uint64_t value = 
                ((uint64_t)(round(doubleBuffer[globalIndex]))) | 
                ((uint64_t)(round(doubleBuffer[globalIndex + n])) << 16) |
                ((uint64_t)(round(doubleBuffer[globalIndex + n * 2])) << 32) |
                ((uint64_t)(round(doubleBuffer[globalIndex + n * 3])) << 48);
            intBuffer[globalIndex] = value;
        }
    }

    void hostUint64AsDoubleMatmulStrided(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {

        // create device memory
        size_t sizeA = M * K;
        size_t sizeB = K * N;
        size_t sizeC = M * N;
        size_t total = sizeA + sizeB + sizeC;
        Integer* intBuffer = allocateDeviceArray<uint64_t>(total);
        double* doubleBuffer = allocateDeviceArray<double>(total * 4 + sizeC);
        Integer* intA = intBuffer;
        Integer* intB = intBuffer + sizeA;
        Integer* intC = intBuffer + sizeA + sizeB;
        double* doubleA = doubleBuffer;
        double* doubleB = doubleBuffer + sizeA * 4;
        double* doubleC = doubleBuffer + (sizeA + sizeB) * 4;
        double* temp = doubleBuffer + total * 4;

        // copy to device memory
        copyToDeviceStrided(intA, A, strideA, sizeA);
        copyToDeviceStrided(intB, B, strideB, sizeB);

        // decompose into double
        KERNEL_CALL(decomposeUint64, sizeA)(doubleA, intA, sizeA);
        KERNEL_CALL(decomposeUint64, sizeB)(doubleB, intB, sizeB);

        // matrix multiplication
        // - 0~16 bits
        double* targetC = doubleC;
        deviceDoubleMatmul(N, M, K, doubleB, doubleA, doubleC);
        // - 16~32 bits
        targetC = doubleC + sizeC;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA, targetC);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC);
        // - 32~64 bits
        targetC = doubleC + sizeC * 2;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC);
        // - 48~64 bits
        targetC = doubleC + sizeC * 3;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 2);
        // overflow
        deviceArrayReduce16Inplace(sizeC, targetC);

        // compose back to int
        KERNEL_CALL(composeUint64, sizeC)(intC, doubleC, sizeC);

        // copy back to host memory
        copyToHostStrided(C, intC, strideC, sizeC);

        // free device memory
        freeDeviceArray(intBuffer, total);
        freeDeviceArray(doubleBuffer, total * 4 + sizeC);
    }    

}