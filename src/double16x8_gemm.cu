#include "include/int-gemm/int_gemm.h"
#include "include/int-gemm/utils_cuda.cuh"

#define KERNEL_CALL(funcname, n) funcname<<<((n) + 255) / 256, 256>>>
#define GLOBAL_INDEX (blockDim.x * blockIdx.x + threadIdx.x)

namespace int_gemm {

    using uint128_t = unsigned __int128;
    using Integer = uint128_t;

    // 0~16 bit of intBuffer[i] to doubleBuffer[i]
    // 16~32 bit of intBuffer[i] to doubleBuffer[i + n]
    // 32~48 bit of intBuffer[i] to doubleBuffer[i + 2n]
    // 48~64 bit of intBuffer[i] to doubleBuffer[i + 3n]
    // 64~80 bit of intBuffer[i] to doubleBuffer[i + 4n] ...
    __global__ void decomposeUint128(double* doubleBuffer, uint128_t* intBuffer, size_t n) {
        size_t globalIndex = GLOBAL_INDEX;
        if (globalIndex < n) {
            uint128_t value = intBuffer[globalIndex];
            doubleBuffer[globalIndex] = (double)(value & 0xFFFF);
            doubleBuffer[globalIndex + n] = (double)((value >> 16) & 0xFFFF);
            doubleBuffer[globalIndex + n * 2] = (double)((value >> 32) & 0xFFFF);
            doubleBuffer[globalIndex + n * 3] = (double)((value >> 48) & 0xFFFF);
            doubleBuffer[globalIndex + n * 4] = (double)((value >> 64) & 0xFFFF);
            doubleBuffer[globalIndex + n * 5] = (double)((value >> 80) & 0xFFFF);
            doubleBuffer[globalIndex + n * 6] = (double)((value >> 96) & 0xFFFF);
            doubleBuffer[globalIndex + n * 7] = (double)((value >> 112) & 0xFFFF);
        }
    }

    // Inverse of decomposeUint128
    __global__ void composeUint128(uint128_t* intBuffer, double* doubleBuffer, size_t n) {
        size_t globalIndex = GLOBAL_INDEX;
        if (globalIndex < n) {
            uint128_t value = 
                ((uint128_t)(round(doubleBuffer[globalIndex]))) | 
                ((uint128_t)(round(doubleBuffer[globalIndex + n])) << 16) |
                ((uint128_t)(round(doubleBuffer[globalIndex + n * 2])) << 32) |
                ((uint128_t)(round(doubleBuffer[globalIndex + n * 3])) << 48) |
                ((uint128_t)(round(doubleBuffer[globalIndex + n * 4])) << 64) |
                ((uint128_t)(round(doubleBuffer[globalIndex + n * 5])) << 80) |
                ((uint128_t)(round(doubleBuffer[globalIndex + n * 6])) << 96) |
                ((uint128_t)(round(doubleBuffer[globalIndex + n * 7])) << 112);
            intBuffer[globalIndex] = value;
        }
    }

    void hostUint128AsDoubleMatmulStrided(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {

        // create device memory
        size_t sizeA = M * K;
        size_t sizeB = K * N;
        size_t sizeC = M * N;
        size_t total = sizeA + sizeB + sizeC;
        Integer* intBuffer = allocateDeviceArray<uint128_t>(total);
        double* doubleBuffer = allocateDeviceArray<double>(total * 8 + sizeC);
        Integer* intA = intBuffer;
        Integer* intB = intBuffer + sizeA;
        Integer* intC = intBuffer + sizeA + sizeB;
        double* doubleA = doubleBuffer;
        double* doubleB = doubleBuffer + sizeA * 8;
        double* doubleC = doubleBuffer + (sizeA + sizeB) * 8;
        double* temp = doubleBuffer + total * 8;

        // copy to device memory
        copyToDeviceStrided(intA, A, strideA, sizeA);
        copyToDeviceStrided(intB, B, strideB, sizeB);

        // decompose into double
        KERNEL_CALL(decomposeUint128, sizeA)(doubleA, intA, sizeA);
        KERNEL_CALL(decomposeUint128, sizeB)(doubleB, intB, sizeB);

        // matrix multiplication
        // - 0~16 bits
        double* targetC = doubleC;
        deviceDoubleMatmul(N, M, K, doubleB, doubleA, doubleC);
        // - 16~32 bits
        targetC = doubleC + sizeC;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 0);
        // - 32~64 bits
        targetC = doubleC + sizeC * 2;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 1);
        // - 48~64 bits
        targetC = doubleC + sizeC * 3;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 2);
        // - 64~80 bits
        targetC = doubleC + sizeC * 4;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 3);
        // - 80~96 bits
        targetC = doubleC + sizeC * 5;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 5, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 5, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 4);
        // - 96~112 bits
        targetC = doubleC + sizeC * 6;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 6, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 5, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 5, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 6, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 5);
        // - 112~128 bits
        targetC = doubleC + sizeC * 7;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 7, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 6, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 5, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 5, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 6, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 7, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 6);
        // - overflow
        deviceArrayReduce16Inplace(sizeC, targetC);

        // compose back to int
        KERNEL_CALL(composeUint128, sizeC)(intC, doubleC, sizeC);

        // copy back to host memory
        copyToHostStrided(C, intC, strideC, sizeC);

        // free device memory
        freeDeviceArray(intBuffer, total);
        freeDeviceArray(doubleBuffer, total * 8 + sizeC);
    }    

}