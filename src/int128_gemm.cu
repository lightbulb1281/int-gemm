#include "include/int-gemm/int_gemm.h"
#include "include/int-gemm/int64x2_gemm.cuh"
#include "cutlass/gemm/device/gemm.h"
#include "include/int-gemm/utils_cuda.cuh"

namespace int_gemm {

    typedef __int128_t int128_t;
    using Integer = int128_t;

    // The matrices are in row-major format.
    // They are copied to the GPU memory to perform device matrix multiplication.
    void hostInt128MatmulStrided(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {
        size_t mul = M * N * K;
        bool strided = strideA != 1 || strideB != 1;
        bool cpu = !cudaAvailable() 
                || (strided && mul < INT128_STRIDED_THRESHOLD)
                || (!strided && mul < INT128_CONSECUTIVE_THRESHOLD);
        if (cpu) {
            cpuIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
            return;
        }
        // create device memory and copy
        Integer* deviceBuffer = allocateDeviceArray<Integer>(M * K + K * N + M * N);
        Integer* deviceA = deviceBuffer;
        Integer* deviceB = deviceBuffer + M * K;
        Integer* deviceC = deviceBuffer + M * K + K * N;
        copyToDeviceStrided(deviceA, A, strideA, M * K);
        copyToDeviceStrided(deviceB, B, strideB, K * N);
        // perform matrix multiplication
        deviceUint64x2Matmul(N, M, K, 
            reinterpret_cast<const uint64x2_t*>(deviceB), 
            reinterpret_cast<const uint64x2_t*>(deviceA), 
            reinterpret_cast<uint64x2_t*>(deviceC)
        );
        // copy back to host memory
        copyToHostStrided(C, deviceC, strideC, M * N);
        // free device memory
        freeDeviceArray(deviceBuffer, M * K + K * N + M * N);
    }    
    
    // The matrices are in row-major format.
    void hostInt128Matmul(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C
    ) {
        hostInt128MatmulStrided(M, N, K, A, B, C, 1, 1, 1);
    }

}