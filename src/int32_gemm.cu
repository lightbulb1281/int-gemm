#include "include/int-gemm/int_gemm.h"
#include "cutlass/gemm/device/gemm.h"
#include "include/int-gemm/utils_cuda.cuh"

namespace int_gemm {

    using Integer = int32_t;

    cudaError_t cutlassInt32Matmul(
        int M,
        int N,
        int K,
        Integer alpha,
        Integer const *A,
        int lda,
        Integer const *B,
        int ldb,
        Integer beta,
        Integer *C,
        int ldc
    ) {
        using ColumnMajor = cutlass::layout::ColumnMajor;
        using CutlassGemm = cutlass::gemm::device::Gemm<
            Integer, ColumnMajor,
            Integer, ColumnMajor,
            Integer, ColumnMajor
        >;
        CutlassGemm gemm_operator;
        CutlassGemm::Arguments args(
            {M, N, K}, 
            {A, lda}, 
            {B, ldb}, 
            {C, ldc}, 
            {C, ldc}, 
            {alpha, beta}
        );
        cutlass::Status status = gemm_operator(args);
        if (status != cutlass::Status::kSuccess) {
            return cudaErrorUnknown;
        }
        return cudaSuccess;
    }

    // The inputs A, B and C should already be on the GPU memory
    // and they are in column-major format.
    void deviceInt32Matmul(
        size_t M, size_t N, size_t K,
        const Integer *A, const Integer *B, Integer *C
    ) {
        cutlassInt32Matmul(
            (int)M, (int)N, (int)K, 1, A, (int)M, B, (int)K, 0, C, (int)M
        );
    }

    // The matrices are in row-major format.
    // They are copied to the GPU memory to perform device matrix multiplication.
    void hostInt32MatmulStrided(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {
        size_t mul = M * N * K;
        bool strided = strideA != 1 || strideB != 1;
        bool cpu = !cudaAvailable() 
                || (strided && mul < INT32_STRIDED_THRESHOLD)
                || (!strided && mul < INT32_CONSECUTIVE_THRESHOLD);
        if (cpu) {
            cpuIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
            return;
        }
        // create device memory and copy
        Integer* deviceBuffer = allocateDeviceArray<Integer>(M * K + K * N + M * N);
        Integer* deviceA = deviceBuffer;
        Integer* deviceB = deviceBuffer + M * K;
        Integer* deviceC = deviceBuffer + M * K + K * N;
        copyToDeviceStrided(deviceA, A, strideA, M * K);
        copyToDeviceStrided(deviceB, B, strideB, K * N);
        // perform matrix multiplication
        deviceInt32Matmul(N, M, K, deviceB, deviceA, deviceC);
        // copy back to host memory
        copyToHostStrided(C, deviceC, strideC, M * N);
        // free device memory
        freeDeviceArray(deviceBuffer, M * K + K * N + M * N);
    }    
    
    // The matrices are in row-major format.
    void hostInt32Matmul(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C
    ) {
        hostInt32MatmulStrided(M, N, K, A, B, C, 1, 1, 1);
    }

}