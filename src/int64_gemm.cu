#include "include/int-gemm/int_gemm.h"
#include "cutlass/gemm/device/gemm.h"
#include "include/int-gemm/utils_cuda.cuh"

namespace int_gemm {

    using Integer = int64_t;

    // NN means both A and B is not transposed. That is: C = A * B.
    cudaError_t cutlassInt64Matmul(
        int M,
        int N,
        int K,
        Integer alpha,
        Integer const *A,
        int lda,
        Integer const *B,
        int ldb,
        Integer beta,
        Integer *C,
        int ldc
    ) {
        using ColumnMajor = cutlass::layout::ColumnMajor;
        using CutlassGemm = cutlass::gemm::device::Gemm<
            Integer, ColumnMajor,
            Integer, ColumnMajor,
            Integer, ColumnMajor
        >;
        CutlassGemm gemm_operator;
        CutlassGemm::Arguments args(
            {M, N, K}, 
            {A, lda}, 
            {B, ldb}, 
            {C, ldc}, 
            {C, ldc}, 
            {alpha, beta}
        );
        cutlass::Status status = gemm_operator(args);
        if (status != cutlass::Status::kSuccess) {
            return cudaErrorUnknown;
        }
        return cudaSuccess;
    }

    // The inputs A, B and C should already be on the GPU memory
    // and they are in column-major format.
    void deviceInt64Matmul(
        size_t M, size_t N, size_t K,
        const Integer *A, const Integer *B, Integer *C
    ) {
        cutlassInt64Matmul(
            (int)M, (int)N, (int)K, 1, A, (int)M, B, (int)K, 0, C, (int)M
        );
    }

    inline int64_t duration_us(std::chrono::high_resolution_clock::time_point start) {
        return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
    }

    // The matrices are in row-major format.
    // They are copied to the GPU memory to perform device matrix multiplication.
    void hostInt64MatmulStrided(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C,
        size_t strideA, size_t strideB, size_t strideC
    ) {
        auto tick = std::chrono::high_resolution_clock::now();
        size_t mul = M * N * K;
        bool strided = strideA != 1 || strideB != 1;
        bool cpu = !cudaAvailable() 
                || (strided && mul < INT64_STRIDED_THRESHOLD)
                || (!strided && mul < INT64_CONSECUTIVE_THRESHOLD);
        if (cpu) {
            cpuIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);

            auto duration = duration_us(tick);
            printf("64 bit cpu, m=%lu, n=%lu, k=%lu, stride=(%lu, %lu, %lu), time=%lld\n", M, N, K, strideA, strideB, strideC, duration);

            return;
        }
        // create device memory and copy

        auto start = std::chrono::high_resolution_clock::now();
        Integer* deviceBuffer = allocateDeviceArray<Integer>(M * K + K * N + M * N);
        Integer* deviceA = deviceBuffer;
        Integer* deviceB = deviceBuffer + M * K;
        Integer* deviceC = deviceBuffer + M * K + K * N;
        auto duration_allocate = duration_us(start);

        start = std::chrono::high_resolution_clock::now();
        copyToDeviceStrided(deviceA, A, strideA, M * K);
        copyToDeviceStrided(deviceB, B, strideB, K * N);
        auto duration_copy2dev = duration_us(start);

        // perform matrix multiplication
        start = std::chrono::high_resolution_clock::now();
        deviceInt64Matmul(N, M, K, deviceB, deviceA, deviceC);
        auto duration_mul = duration_us(start);

        // copy back to host memory
        start = std::chrono::high_resolution_clock::now();
        copyToHostStrided(C, deviceC, strideC, M * N);
        auto duration_copy2host = duration_us(start);

        // free device memory
        freeDeviceArray(deviceBuffer, M * K + K * N + M * N);

        auto duration = duration_us(tick);
        printf("64 bit, m=%lu, n=%lu, k=%lu, stride=(%lu, %lu, %lu), time = (alc %lld + cpd %lld + mul %lld + cph %lld) = %lld\n",
            M, N, K, strideA, strideB, strideC,
            duration_allocate, duration_copy2dev, duration_mul, duration_copy2host, duration);
    }    
    
    // The matrices are in row-major format.
    void hostInt64Matmul(
        size_t M, size_t N, size_t K,
        const Integer* A, const Integer* B, Integer* C
    ) {
        hostInt64MatmulStrided(M, N, K, A, B, C, 1, 1, 1);
    }

}