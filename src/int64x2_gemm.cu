#include "include/int-gemm/int64x2_gemm.cuh"
#include "cutlass/gemm/device/gemm.h"

namespace int_gemm {

    __host__ __device__
    uint64x2_t::uint64x2_t() : lo(0), hi(0) {}

    __host__ __device__
    uint64x2_t::uint64x2_t(uint64_t hi, uint64_t lo) : lo(lo), hi(hi) {}

    uint64x2_t::uint64x2_t(__uint128_t val) {
        *this = *(reinterpret_cast<uint64x2_t*>(&val));
    }

    __uint128_t uint64x2_t::to_uint128() const {
        return *(reinterpret_cast<const __uint128_t*>(this));
    }

    __host__ __device__
    uint64x2_t uint64x2_t::operator+(const uint64x2_t& rhs) const {
        uint64_t lo = this->lo + rhs.lo;
        uint64_t carry = (uint64_t)(lo < this->lo);
        uint64_t hi = this->hi + rhs.hi + carry;
        return uint64x2_t(hi, lo);
    }

    __host__ __device__
    uint64x2_t uint64x2_t::operator-(const uint64x2_t& rhs) const {
        uint64_t lo = this->lo - rhs.lo;
        uint64_t borrow = (uint64_t)(lo > this->lo);
        uint64_t hi = this->hi - rhs.hi - borrow;
        return uint64x2_t(hi, lo);
    }

    __device__ __host__
    uint64x2_t uint64x2_t::operator*(const uint64x2_t& rhs) const {

        #ifdef __CUDA_ARCH__
            uint64_t lo = this->lo * rhs.lo;
            uint64_t hi = __umul64hi(this->lo, rhs.lo);
        #else
            __uint128_t lom128 = (__uint128_t)(this->lo) * (__uint128_t)(rhs.lo);
            uint64_t lo = (uint64_t)(lom128 & 0xFFFFFFFFFFFFFFFF);
            uint64_t hi = (uint64_t)(lom128 >> 64);
        #endif

        hi += this->lo * rhs.hi;
        hi += this->hi * rhs.lo;
        return uint64x2_t(hi, lo);

    }

    using Integer = uint64x2_t;

    // NN means both A and B is not transposed. That is: C = A * B.
    cudaError_t cutlassUint64x2Matmul(
        int M,
        int N,
        int K,
        Integer alpha,
        Integer const *A,
        int lda,
        Integer const *B,
        int ldb,
        Integer beta,
        Integer *C,
        int ldc
    ) {
        using ColumnMajor = cutlass::layout::ColumnMajor;
        using CutlassGemm = cutlass::gemm::device::Gemm<
            Integer, ColumnMajor,
            Integer, ColumnMajor,
            Integer, ColumnMajor
        >;
        CutlassGemm gemm_operator;
        CutlassGemm::Arguments args(
            {M, N, K}, 
            {A, lda}, 
            {B, ldb}, 
            {C, ldc}, 
            {C, ldc}, 
            {alpha, beta}
        );
        cutlass::Status status = gemm_operator(args);
        if (status != cutlass::Status::kSuccess) {
            return cudaErrorUnknown;
        }
        return cudaSuccess;
    }

    // The inputs A, B and C should already be on the GPU memory
    // and they are in column-major format.
    void deviceUint64x2Matmul(
        size_t M, size_t N, size_t K,
        const Integer *A, const Integer *B, Integer *C
    ) {
        cutlassUint64x2Matmul(
            (int)M, (int)N, (int)K, uint64x2_t(1), A, (int)M, B, (int)K, uint64x2_t(0), C, (int)M
        );
    }

}