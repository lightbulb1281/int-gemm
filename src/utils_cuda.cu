#include "include/int-gemm/utils_cuda.cuh"
#include "cutlass/gemm/device/gemm.h"

#define KERNEL_CALL(funcname, n) funcname<<<((n) + 255) / 256, 256>>>
#define GLOBAL_INDEX (blockDim.x * blockIdx.x + threadIdx.x)

namespace int_gemm {

    class CudaStreamManager {
        cudaStream_t streams[CUDA_STREAMS_COUNT];
        bool streamsInitialized;
    public:
        CudaStreamManager(): streamsInitialized(false) {}
        cudaStream_t* get() {
            if (!streamsInitialized) {
                for (size_t i = 0; i < CUDA_STREAMS_COUNT; i++) {
                    cudaStreamCreate(&streams[i]);
                }
                streamsInitialized = true;
            }
            return streams;
        }
        ~CudaStreamManager() {
            if (streamsInitialized) {
                for (size_t i = 0; i < CUDA_STREAMS_COUNT; i++) {
                    cudaStreamDestroy(streams[i]);
                }
            }
        }
    } streamManager;

    cudaStream_t* getCudaStreams() {
        return streamManager.get();
    }

    // NN means both A and B is not transposed. That is: C = A * B.
    cudaError_t cutlassDoubleMatmul(
        int M,
        int N,
        int K,
        double alpha,
        double const *A,
        int lda,
        double const *B,
        int ldb,
        double beta,
        double *C,
        int ldc
    ) {
        using ColumnMajor = cutlass::layout::ColumnMajor;
        using CutlassGemm = cutlass::gemm::device::Gemm<
            double, ColumnMajor,
            double, ColumnMajor,
            double, ColumnMajor
        >;
        CutlassGemm gemm_operator;
        CutlassGemm::Arguments args(
            {M, N, K}, 
            {A, lda}, 
            {B, ldb}, 
            {C, ldc}, 
            {C, ldc}, 
            {alpha, beta}
        );
        cutlass::Status status = gemm_operator(args);
        if (status != cutlass::Status::kSuccess) {
            return cudaErrorUnknown;
        }
        return cudaSuccess;
    }

    // The inputs A, B and C should already be on the GPU memory
    // and they are in column-major format.
    void deviceDoubleMatmul(
        size_t M, size_t N, size_t K,
        const double *A, const double *B, double *C
    ) {
        cutlassDoubleMatmul(
            (int)M, (int)N, (int)K, 1, A, (int)M, B, (int)K, 0, C, (int)M
        );
    }

    __global__ void deviceArrayAddInplaceKernel(
        size_t size,
        double *A, const double *B
    ) {
        size_t idx = GLOBAL_INDEX;
        if (idx < size) {
            A[idx] += B[idx];
        }
    }

    void deviceArrayAddInplace(
        size_t size,
        double *A, const double *B
    ) {
        KERNEL_CALL(deviceArrayAddInplaceKernel, size)(size, A, B);
    }

    // A += (int)(B / (1<<16)); B = B % (1<<16)
    __global__ void deviceArrayCarry16Kernel(
        size_t size,
        double *A, double *B
    ) {
        size_t idx = GLOBAL_INDEX;
        if (idx < size) {
            A[idx] += floor(B[idx] / 65536.0);
            B[idx] = fmod(B[idx], 65536.0);
        }
    }

    // A += (int)(B / (1<<16)); B = B % (1<<16)
    void deviceArrayCarry16(
        size_t size,
        double *A, double *B
    ) {
        KERNEL_CALL(deviceArrayCarry16Kernel, size)(size, A, B);
    }
    
    __global__ void deviceArrayReduce16InplaceKernel(
        size_t size,
        double *A
    ) {
        size_t idx = GLOBAL_INDEX;
        if (idx < size) {
            A[idx] = fmod(A[idx], 65536.0);
        }
    }
    
    // A = A % (1<<16)
    void deviceArrayReduce16Inplace(
        size_t size, double *A
    ) {
        KERNEL_CALL(deviceArrayReduce16InplaceKernel, size)(size, A);
    }

}