#include <cstdint>
#include <string>
#include <chrono>
#include <iostream>
#include <vector>
#include <cstdlib>

using TimePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;
using Duration = std::chrono::microseconds;

struct TimeRecord {

    Duration firstDuration;
    Duration subsequentDurations;
    TimePoint lastTick;
    size_t tickedTimes;
    std::string name;

    TimeRecord(std::string name) : name(name) {
        tickedTimes = 0;
        subsequentDurations = Duration(0);
    }

    void tick() {
        lastTick = std::chrono::high_resolution_clock::now();
    }

    void tock() {
        auto end_time = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - lastTick);
        if (tickedTimes > 0) {
            subsequentDurations += duration;
        } else {
            firstDuration = duration;
        }
        tickedTimes += 1;
    }

    void print() {
        // format:
        // [Name] [AverageTime = SubsequentDuration / (TickedTimes - 1)] us ([FirstTime] us)
        double averageSubsequentDuration = (double)subsequentDurations.count() / (tickedTimes - 1);
        std::cout << name 
            << " " << averageSubsequentDuration 
            // << " = " << subsequentDurations.count() << " / " << (tickedTimes - 1)
            << " us (first-run " << firstDuration.count() << " us)" 
            << std::endl;
    }

};

class Timer {

private:

    std::vector<TimeRecord> records;

public:

    Timer() {}

    size_t addTimer(std::string name) {
        records.push_back(TimeRecord(name));
        return records.size() - 1;
    }

    void tick(size_t handle) {
        records[handle].tick();
    }

    void tock(size_t handle) {
        records[handle].tock();
    }
    
    void printAll() {
        for (auto& record : records) {
            record.print();
        }
    }

};

void directCopy(int n, int stride, int repeatTimes = 16) {

    std::cout << "[DirectCopy] n = " << n << ", stride = " << stride << std::endl;

    Timer timer;
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tCopyToHost = timer.addTimer("CopyToHost  ");

    // create an int array of n * stride
    int* arr = new int[n * stride];

    // fill with random numbers
    for (int i = 0; i < n * stride; i++) {
        arr[i] = rand();
    }

    // create cuda buffer of length n
    int* cudaBuffer;
    cudaError_t result;
    result = cudaMalloc(&cudaBuffer, n * sizeof(int));
    if (result != cudaSuccess) {
        throw std::runtime_error("Failed to allocate cuda buffer");
    }

    // copy to cuda buffer
    for (size_t rep = 0; rep < repeatTimes; rep++) {
        timer.tick(tCopyToDevice);
        if (stride == 1) {
            result = cudaMemcpy(cudaBuffer, arr, n * sizeof(int), cudaMemcpyHostToDevice);
        } else {
            result = cudaMemcpy2D(cudaBuffer, sizeof(int), arr, stride * sizeof(int), sizeof(int), n, cudaMemcpyHostToDevice);
        }
        if (result != cudaSuccess) {
            throw std::runtime_error("Failed to copy to device");
        }
        timer.tock(tCopyToDevice);

        // copy back
        timer.tick(tCopyToHost);
        if (stride == 1) {
            result = cudaMemcpy(arr, cudaBuffer, n * sizeof(int), cudaMemcpyDeviceToHost);
        } else {
            result = cudaMemcpy2D(arr, stride * sizeof(int), cudaBuffer, sizeof(int), sizeof(int), n, cudaMemcpyDeviceToHost);
        }
        if (result != cudaSuccess) {
            throw std::runtime_error("Failed to copy to host");
        }
        timer.tock(tCopyToHost);
    }

    // delete buffer
    cudaFree(cudaBuffer);
    delete[] arr;

    // print timer
    timer.printAll();

}

void asyncCopy(int n, int stride, int streams = 4, int repeatTimes = 16) {

    std::cout << "[AsyncCopy] n = " << n << ", stride = " << stride << std::endl;

    Timer timer;
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tCopyToHost = timer.addTimer("CopyToHost  ");

    // create an int array of n * stride
    int* arr = new int[n * stride];

    // fill with random numbers
    for (int i = 0; i < n * stride; i++) {
        arr[i] = rand();
    }

    // create cuda buffer of length n
    int* cudaBuffer;
    cudaError_t result;
    result = cudaMalloc(&cudaBuffer, n * sizeof(int));
    if (result != cudaSuccess) {
        throw std::runtime_error("Failed to allocate cuda buffer");
    }

    // create streams
    cudaStream_t* stream = new cudaStream_t[streams];
    for (int i = 0; i < streams; i++) {
        result = cudaStreamCreate(&stream[i]);
        if (result != cudaSuccess) {
            throw std::runtime_error("Failed to create stream");
        }
    }

    // copy to cuda buffer
    int interval = n / streams;
    for (size_t rep = 0; rep < repeatTimes; rep++) {

        timer.tick(tCopyToDevice);
        for (int s = 0; s < streams; s++) {
            int lower = s * interval;
            int count = (s == streams - 1) ? (n - lower) : interval;
            if (stride == 1) {
                result = cudaMemcpyAsync(
                    cudaBuffer + lower, 
                    arr + lower, 
                    count * sizeof(int), cudaMemcpyHostToDevice, stream[s]
                );
            } else {
                result = cudaMemcpy2DAsync(
                    cudaBuffer + lower, sizeof(int), 
                    arr + lower * stride, stride * sizeof(int), 
                    sizeof(int), count, cudaMemcpyHostToDevice, stream[s]
                );
            }
            if (result != cudaSuccess) {
                throw std::runtime_error("Failed to copy to device");
            }
        }
        cudaDeviceSynchronize();
        timer.tock(tCopyToDevice);

        // copy back
        timer.tick(tCopyToHost);
        for (int s = 0; s < streams; s++) {
            int lower = s * interval;
            int count = (s == streams - 1) ? (n - lower) : interval;
            if (stride == 1) {
                result = cudaMemcpyAsync(
                    arr + lower, 
                    cudaBuffer + lower, 
                    count * sizeof(int), cudaMemcpyDeviceToHost, stream[s]
                );
            } else {
                result = cudaMemcpy2DAsync(
                    arr + lower * stride, stride * sizeof(int), 
                    cudaBuffer + lower, sizeof(int), 
                    sizeof(int), count, cudaMemcpyDeviceToHost, stream[s]
                );
            }
            if (result != cudaSuccess) {
                throw std::runtime_error("Failed to copy to host");
            }
        }
        cudaDeviceSynchronize();
        timer.tock(tCopyToHost);

    }

    // release streams
    for (int i = 0; i < streams; i++) {
        cudaStreamDestroy(stream[i]);
    }
    delete[] stream;

    // delete buffer
    cudaFree(cudaBuffer);
    delete[] arr;

    // print timer
    timer.printAll(); 

}

int main() {
    directCopy(512 * 512, 1, 16);
    directCopy(512 * 512, 2, 16);
    asyncCopy(512 * 512, 1, 4);
    asyncCopy(512 * 512, 2, 4);
    return 0;
}