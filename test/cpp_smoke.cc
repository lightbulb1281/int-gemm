#include <stdint.h>
#include "../include/int-gemm/int_gemm.h"

int main() {

    int32_t a[4] = {1, 2, 3, 4};
    int32_t b[4] = {1, 2, 3, 4};
    int32_t c[4];

    int_gemm::hostInt32Matmul(2, 2, 2, a, b, c);

    return 0;

}