#include "../include/int-gemm/int_gemm.h"
#include <gtest/gtest.h>
#include "utils.h"

typedef __int128_t Integer;

void referenceInt128Matmul(        
    const size_t M,
    const size_t N,
    const size_t K,
    const Integer *A,
    const Integer *B,
    Integer *C
) {
    for (size_t m = 0; m < M; ++m) {
        for (size_t n = 0; n < N; ++n) {
            Integer sum = 0;
            for (size_t k = 0; k < K; ++k) {
                sum += A[m * K + k] * B[k * N + n];
            }
            C[m * N + n] = sum;
        }
    }
}

void referenceInt128MatmulStrided( 
    const size_t M,
    const size_t N,
    const size_t K,
    const Integer *A,
    const Integer *B,
    Integer *C,
    size_t strideA,
    size_t strideB,
    size_t strideC
) {
    for (size_t m = 0; m < M; ++m) {
        for (size_t n = 0; n < N; ++n) {
            Integer sum = 0;
            for (size_t k = 0; k < K; ++k) {
                sum += A[(m * K + k) * strideA] * B[(k * N + n) * strideB];
            }
            C[(m * N + n) * strideC] = sum;
        }
    }
}

TEST(Int128Test, HostMatmul) {

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;

    Integer* hostA = new Integer[M * K];
    Integer* hostB = new Integer[K * N];
    Integer* hostC = new Integer[M * N];

    randomInt128(hostA, M * K);
    randomInt128(hostB, K * N);

    referenceInt128Matmul(M, N, K, hostA, hostB, hostC);

    Integer* computeC = new Integer[M * N];
    int_gemm::hostInt128Matmul(M, N, K, hostA, hostB, computeC);

    for (size_t i = 0; i < M * N; ++i) {
        ASSERT_EQ(hostC[i], computeC[i]);
    }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] computeC;

}



TEST(Int128Test, HostMatmulStrided) {

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;
    size_t strideA = 2;
    size_t strideB = 3;
    size_t strideC = 4;

    Integer* hostA = new Integer[M * K * strideA];
    Integer* hostB = new Integer[K * N * strideB];
    Integer* hostC = new Integer[M * N * strideC];

    randomInt128(hostA, M * K * strideA);
    randomInt128(hostB, K * N * strideB);

    referenceInt128MatmulStrided(M, N, K, hostA, hostB, hostC, strideA, strideB, strideC);

    Integer* computeC = new Integer[M * N * strideC];
    int_gemm::hostInt128MatmulStrided(M, N, K, hostA, hostB, computeC, strideA, strideB, strideC);

    for (size_t i = 0; i < M * N * strideC; i += strideC) {
        ASSERT_EQ(hostC[i], computeC[i]);
    }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] computeC;

}


TEST(UInt128Test, HostMatmulAsDoubleStrided) {

    using uint128_t = __uint128_t;
    using int128_t = __int128_t;

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;
    size_t strideA = 2;
    size_t strideB = 3;
    size_t strideC = 4;

    uint128_t* hostA = new uint128_t[M * K * strideA];
    uint128_t* hostB = new uint128_t[K * N * strideB];
    uint128_t* hostC = new uint128_t[M * N * strideC];

    randomInt128(reinterpret_cast<int128_t*>(hostA), M * K * strideA);
    randomInt128(reinterpret_cast<int128_t*>(hostB), K * N * strideB);

    referenceInt128MatmulStrided(
        M, N, K, 
        reinterpret_cast<const int128_t*>(hostA), 
        reinterpret_cast<const int128_t*>(hostB), 
        reinterpret_cast<int128_t*>(hostC), 
        strideA, strideB, strideC
    );

    uint128_t* computeC = new uint128_t[M * N * strideC];
    int_gemm::hostUint128AsDoubleMatmulStrided(M, N, K, hostA, hostB, computeC, strideA, strideB, strideC);

    for (size_t i = 0; i < M * N * strideC; i += strideC) {
        ASSERT_EQ(hostC[i], computeC[i]);
    }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] computeC;

}