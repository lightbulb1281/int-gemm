#include "../include/int-gemm/int_gemm.h"
#include "../include/int-gemm/utils_cuda.cuh"
#include <gtest/gtest.h>
#include "utils.h"

typedef int64_t Integer;

void referenceInt64Matmul(        
    const size_t M,
    const size_t N,
    const size_t K,
    const Integer *A,
    const Integer *B,
    Integer *C
) {
    for (size_t m = 0; m < M; ++m) {
        for (size_t n = 0; n < N; ++n) {
            Integer sum = 0;
            for (size_t k = 0; k < K; ++k) {
                sum += A[m * K + k] * B[k * N + n];
            }
            C[m * N + n] = sum;
        }
    }
}

void referenceInt64MatmulStrided( 
    const size_t M,
    const size_t N,
    const size_t K,
    const Integer *A,
    const Integer *B,
    Integer *C,
    size_t strideA,
    size_t strideB,
    size_t strideC
) {
    for (size_t m = 0; m < M; ++m) {
        for (size_t n = 0; n < N; ++n) {
            Integer sum = 0;
            for (size_t k = 0; k < K; ++k) {
                sum += A[(m * K + k) * strideA] * B[(k * N + n) * strideB];
            }
            C[(m * N + n) * strideC] = sum;
        }
    }
}

TEST(Int64Test, DeviceMatmul) {

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;

    Integer* hostA = new Integer[M * K];
    Integer* hostB = new Integer[K * N];
    Integer* hostC = new Integer[M * N];

    randomInt64(hostA, M * K);
    randomInt64(hostB, K * N);

    // Semantically device matmul uses C^T = B^T * A^T 
    // So we symmetrically let reference use transposed inputs.
    referenceInt64Matmul(N, M, K, hostB, hostA, hostC);

    Integer* deviceA = int_gemm::allocateDeviceArray<Integer>(M * K);
    Integer* deviceB = int_gemm::allocateDeviceArray<Integer>(K * N);
    Integer* deviceC = int_gemm::allocateDeviceArray<Integer>(M * N);

    int_gemm::copyToDevice(deviceA, hostA, M * K);
    int_gemm::copyToDevice(deviceB, hostB, K * N);

    int_gemm::deviceInt64Matmul(M, N, K, deviceA, deviceB, deviceC);

    Integer* retrievedC = new Integer[M * N];
    int_gemm::copyToHost(retrievedC, deviceC, M * N);

    for (size_t i = 0; i < M * N; ++i) {
        ASSERT_EQ(hostC[i], retrievedC[i]);
    }

    int_gemm::freeDeviceArray(deviceA, M * K);
    int_gemm::freeDeviceArray(deviceB, K * N);
    int_gemm::freeDeviceArray(deviceC, M * N);

    // print hostc. try `ctest -R Matmul -V`
    // for (size_t i = 0; i < M * N; ++i) {
    //     std::cout << hostC[i] << " ";
    // }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] retrievedC;

}

TEST(Int64Test, HostMatmul) {

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;

    Integer* hostA = new Integer[M * K];
    Integer* hostB = new Integer[K * N];
    Integer* hostC = new Integer[M * N];

    randomInt64(hostA, M * K);
    randomInt64(hostB, K * N);

    referenceInt64Matmul(M, N, K, hostA, hostB, hostC);

    Integer* computeC = new Integer[M * N];
    int_gemm::hostInt64Matmul(M, N, K, hostA, hostB, computeC);

    for (size_t i = 0; i < M * N; ++i) {
        ASSERT_EQ(hostC[i], computeC[i]);
    }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] computeC;

}



TEST(Int64Test, HostMatmulStrided) {

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;
    size_t strideA = 2;
    size_t strideB = 3;
    size_t strideC = 4;

    Integer* hostA = new Integer[M * K * strideA];
    Integer* hostB = new Integer[K * N * strideB];
    Integer* hostC = new Integer[M * N * strideC];

    randomInt64(hostA, M * K * strideA);
    randomInt64(hostB, K * N * strideB);

    referenceInt64MatmulStrided(M, N, K, hostA, hostB, hostC, strideA, strideB, strideC);

    Integer* computeC = new Integer[M * N * strideC];
    int_gemm::hostInt64MatmulStrided(M, N, K, hostA, hostB, computeC, strideA, strideB, strideC);

    for (size_t i = 0; i < M * N * strideC; i += strideC) {
        ASSERT_EQ(hostC[i], computeC[i]);
    }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] computeC;

}


TEST(UInt64Test, HostMatmulAsDoubleStrided) {

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;
    size_t strideA = 2;
    size_t strideB = 3;
    size_t strideC = 4;

    uint64_t* hostA = new uint64_t[M * K * strideA];
    uint64_t* hostB = new uint64_t[K * N * strideB];
    uint64_t* hostC = new uint64_t[M * N * strideC];

    randomInt32(reinterpret_cast<int32_t*>(hostA), M * K * strideA);
    randomInt32(reinterpret_cast<int32_t*>(hostB), K * N * strideB);

    referenceInt64MatmulStrided(
        M, N, K, 
        reinterpret_cast<int64_t*>(hostA), reinterpret_cast<int64_t*>(hostB), reinterpret_cast<int64_t*>(hostC), 
        strideA, strideB, strideC
    );

    uint64_t* computeC = new uint64_t[M * N * strideC];
    int_gemm::hostUint64AsDoubleMatmulStrided(M, N, K, hostA, hostB, computeC, strideA, strideB, strideC);

    for (size_t i = 0; i < M * N * strideC; i += strideC) {
        ASSERT_EQ(hostC[i], computeC[i]);
    }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] computeC;

}