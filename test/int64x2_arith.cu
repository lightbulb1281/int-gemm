#include "../include/int-gemm/int64x2_gemm.cuh"
#include "../include/int-gemm/utils_cuda.cuh"
#include "../include/int-gemm/int_gemm.h"
#include <gtest/gtest.h>
#include "utils.h"
#include <cstdlib>

using uint128_t = __uint128_t;
using namespace int_gemm;

TEST(Int64x2ArithTest, Addition) {

    for (size_t i = 0; i < 100; i++) {
        uint128_t a = randomUint128();
        uint128_t b = randomUint128();
        uint128_t c = a + b;
        uint64x2_t ad = uint64x2_t(a);
        uint64x2_t bd = uint64x2_t(b);
        uint64x2_t cd = ad + bd;
        uint128_t cd128 = cd.to_uint128();
        ASSERT_EQ(c, cd128);
    }

}

TEST(Int64x2ArithTest, Subtraction) {

    for (size_t i = 0; i < 100; i++) {
        uint128_t a = randomUint128();
        uint128_t b = randomUint128();
        uint128_t c = a - b;
        uint64x2_t ad = uint64x2_t(a);
        uint64x2_t bd = uint64x2_t(b);
        uint64x2_t cd = ad - bd;
        uint128_t cd128 = cd.to_uint128();
        ASSERT_EQ(c, cd128);
    }

}

TEST(Int64x2ArithTest, Multiplication) {

    for (size_t i = 0; i < 100; i++) {
        uint128_t a = randomUint128();
        uint128_t b = randomUint128();
        uint128_t c = a * b;
        uint64x2_t ad = uint64x2_t(a);
        uint64x2_t bd = uint64x2_t(b);
        uint64x2_t cd = ad * bd;
        uint128_t cd128 = cd.to_uint128();
        ASSERT_EQ(c, cd128);
    }

}

void referenceUint128Matmul(        
    const size_t M,
    const size_t N,
    const size_t K,
    const __uint128_t *A,
    const __uint128_t *B,
    __uint128_t *C
) {
    for (size_t m = 0; m < M; ++m) {
        for (size_t n = 0; n < N; ++n) {
            __uint128_t sum = 0;
            for (size_t k = 0; k < K; ++k) {
                sum += A[m * K + k] * B[k * N + n];
            }
            C[m * N + n] = sum;
        }
    }
}

TEST(UInt128Test, DeviceMatmul) {

    using Integer = __int128_t;

    size_t M = 50;
    size_t N = 60;
    size_t K = 70;

    Integer* hostA = new Integer[M * K];
    Integer* hostB = new Integer[K * N];
    Integer* hostC = new Integer[M * N];

    randomUint128(reinterpret_cast<uint128_t*>(hostA), M * K);
    randomUint128(reinterpret_cast<uint128_t*>(hostB), K * N);

    // Semantically device matmul uses C^T = B^T * A^T 
    // So we symmetrically let reference use transposed inputs.
    referenceUint128Matmul(N, M, K, 
        reinterpret_cast<const uint128_t*>(hostB), 
        reinterpret_cast<const uint128_t*>(hostA), 
        reinterpret_cast<uint128_t*>(hostC));

    Integer* deviceA = int_gemm::allocateDeviceArray<Integer>(M * K);
    Integer* deviceB = int_gemm::allocateDeviceArray<Integer>(K * N);
    Integer* deviceC = int_gemm::allocateDeviceArray<Integer>(M * N);

    int_gemm::copyToDevice(deviceA, hostA, M * K);
    int_gemm::copyToDevice(deviceB, hostB, K * N);

    int_gemm::deviceUint64x2Matmul(M, N, K, 
        reinterpret_cast<const uint64x2_t*>(deviceA), 
        reinterpret_cast<const uint64x2_t*>(deviceB), 
        reinterpret_cast<uint64x2_t*>(deviceC));

    Integer* retrievedC = new Integer[M * N];
    int_gemm::copyToHost(retrievedC, deviceC, M * N);

    for (size_t i = 0; i < M * N; ++i) {
        ASSERT_EQ(hostC[i], retrievedC[i]);
    }

    int_gemm::freeDeviceArray(deviceA, M * K);
    int_gemm::freeDeviceArray(deviceB, K * N);
    int_gemm::freeDeviceArray(deviceC, M * N);

    // print hostc. try `ctest -R Matmul -V`
    // for (size_t i = 0; i < M * N; ++i) {
    //     std::cout << hostC[i] << " ";
    // }

    delete[] hostA;
    delete[] hostB;
    delete[] hostC;
    delete[] retrievedC;

}

