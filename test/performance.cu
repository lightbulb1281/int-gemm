#include "../include/int-gemm/int_gemm.h"
#include "../include/int-gemm/int64x2_gemm.cuh"
#include "../include/int-gemm/utils_cuda.cuh"
#include <sys/time.h>
#include <chrono>
#include <iostream>
#include <cstdint>
#include <map>
#include <vector>
#include <string>

#define KERNEL_CALL(funcname, n) funcname<<<((n) + 255) / 256, 256>>>
#define GLOBAL_INDEX (blockDim.x * blockIdx.x + threadIdx.x)

using namespace int_gemm;
using uint128_t = __uint128_t;
using int128_t = __int128_t;
using TimePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;
using Duration = std::chrono::microseconds;

struct TimeRecord {

    Duration firstDuration;
    Duration subsequentDurations;
    TimePoint lastTick;
    size_t tickedTimes;
    std::string name;

    TimeRecord(std::string name) : name(name) {
        tickedTimes = 0;
        subsequentDurations = Duration(0);
    }

    void tick() {
        lastTick = std::chrono::high_resolution_clock::now();
    }

    void tock() {
        auto end_time = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - lastTick);
        if (tickedTimes > 0) {
            subsequentDurations += duration;
        } else {
            firstDuration = duration;
        }
        tickedTimes += 1;
    }

    void print() {
        // format:
        // [Name] [AverageTime = SubsequentDuration / (TickedTimes - 1)] us ([FirstTime] us)
        double averageSubsequentDuration = (double)subsequentDurations.count() / (tickedTimes - 1);
        std::cout << name 
            << " " << averageSubsequentDuration 
            // << " = " << subsequentDurations.count() << " / " << (tickedTimes - 1)
            << " us (first-run " << firstDuration.count() << " us)" 
            << std::endl;
    }

};

class Timer {

private:

    std::vector<TimeRecord> records;

public:

    Timer() {}

    size_t addTimer(std::string name) {
        records.push_back(TimeRecord(name));
        return records.size() - 1;
    }

    void tick(size_t handle) {
        records[handle].tick();
    }

    void tock(size_t handle) {
        records[handle].tock();
    }
    
    void printAll() {
        for (auto& record : records) {
            record.print();
        }
    }

};


void fillBytes(uint8_t *ptr, size_t size) {
    for (size_t i = 0; i < size; i++) {
        ptr[i] = rand() % 256;
    }
}

// Put the lower 16 bits of intBuffer[i] to doubleBuffer[i] and higher 16 bits to doubleBuffer[i + n] 
__global__ void decomposeUint32(double* doubleBuffer, uint32_t* intBuffer, size_t n) {
    size_t globalIndex = GLOBAL_INDEX;
    if (globalIndex < n) {
        uint32_t value = intBuffer[globalIndex];
        doubleBuffer[globalIndex] = (double)(value & 0xFFFF);
        doubleBuffer[globalIndex + n] = (double)(value >> 16);
    }
}

// Inverse of decomposeUint32
__global__ void composeUint32(uint32_t* intBuffer, double* doubleBuffer, size_t n) {
    size_t globalIndex = GLOBAL_INDEX;
    if (globalIndex < n) {
        uint32_t value = 
            ((uint32_t)(round(doubleBuffer[globalIndex]))) | 
            ((uint32_t)(round(doubleBuffer[globalIndex + n])) << 16);
        intBuffer[globalIndex] = value;
    }
}

// 0~16 bit of intBuffer[i] to doubleBuffer[i]
// 16~32 bit of intBuffer[i] to doubleBuffer[i + n]
// 32~48 bit of intBuffer[i] to doubleBuffer[i + 2n]
// 48~64 bit of intBuffer[i] to doubleBuffer[i + 3n]
__global__ void decomposeUint64(double* doubleBuffer, uint64_t* intBuffer, size_t n) {
    size_t globalIndex = GLOBAL_INDEX;
    if (globalIndex < n) {
        uint64_t value = intBuffer[globalIndex];
        doubleBuffer[globalIndex] = (double)(value & 0xFFFF);
        doubleBuffer[globalIndex + n] = (double)((value >> 16) & 0xFFFF);
        doubleBuffer[globalIndex + n * 2] = (double)((value >> 32) & 0xFFFF);
        doubleBuffer[globalIndex + n * 3] = (double)((value >> 48) & 0xFFFF);
    }
}

// Inverse of decomposeUint64
__global__ void composeUint64(uint64_t* intBuffer, double* doubleBuffer, size_t n) {
    size_t globalIndex = GLOBAL_INDEX;
    if (globalIndex < n) {
        uint64_t value = 
            ((uint64_t)(round(doubleBuffer[globalIndex]))) | 
            ((uint64_t)(round(doubleBuffer[globalIndex + n])) << 16) |
            ((uint64_t)(round(doubleBuffer[globalIndex + n * 2])) << 32) |
            ((uint64_t)(round(doubleBuffer[globalIndex + n * 3])) << 48);
        intBuffer[globalIndex] = value;
    }
}

// 0~16 bit of intBuffer[i] to doubleBuffer[i]
// 16~32 bit of intBuffer[i] to doubleBuffer[i + n]
// 32~48 bit of intBuffer[i] to doubleBuffer[i + 2n]
// 48~64 bit of intBuffer[i] to doubleBuffer[i + 3n]
// 64~80 bit of intBuffer[i] to doubleBuffer[i + 4n] ...
__global__ void decomposeUint128(double* doubleBuffer, uint128_t* intBuffer, size_t n) {
    size_t globalIndex = GLOBAL_INDEX;
    if (globalIndex < n) {
        uint128_t value = intBuffer[globalIndex];
        doubleBuffer[globalIndex] = (double)(value & 0xFFFF);
        doubleBuffer[globalIndex + n] = (double)((value >> 16) & 0xFFFF);
        doubleBuffer[globalIndex + n * 2] = (double)((value >> 32) & 0xFFFF);
        doubleBuffer[globalIndex + n * 3] = (double)((value >> 48) & 0xFFFF);
        doubleBuffer[globalIndex + n * 4] = (double)((value >> 64) & 0xFFFF);
        doubleBuffer[globalIndex + n * 5] = (double)((value >> 80) & 0xFFFF);
        doubleBuffer[globalIndex + n * 6] = (double)((value >> 96) & 0xFFFF);
        doubleBuffer[globalIndex + n * 7] = (double)((value >> 112) & 0xFFFF);
    }
}

// Inverse of decomposeUint128
__global__ void composeUint128(uint128_t* intBuffer, double* doubleBuffer, size_t n) {
    size_t globalIndex = GLOBAL_INDEX;
    if (globalIndex < n) {
        uint128_t value = 
            ((uint128_t)(round(doubleBuffer[globalIndex]))) | 
            ((uint128_t)(round(doubleBuffer[globalIndex + n])) << 16) |
            ((uint128_t)(round(doubleBuffer[globalIndex + n * 2])) << 32) |
            ((uint128_t)(round(doubleBuffer[globalIndex + n * 3])) << 48) |
            ((uint128_t)(round(doubleBuffer[globalIndex + n * 4])) << 64) |
            ((uint128_t)(round(doubleBuffer[globalIndex + n * 5])) << 80) |
            ((uint128_t)(round(doubleBuffer[globalIndex + n * 6])) << 96) |
            ((uint128_t)(round(doubleBuffer[globalIndex + n * 7])) << 112);
        intBuffer[globalIndex] = value;
    }
}

template <typename Integer>
void referenceIntMatmulStrided( 
    const size_t M,
    const size_t N,
    const size_t K,
    const Integer *A,
    const Integer *B,
    Integer *C,
    size_t strideA,
    size_t strideB,
    size_t strideC
) {
    for (size_t m = 0; m < M; ++m) {
        for (size_t n = 0; n < N; ++n) {
            Integer sum = Integer(0);
            for (size_t k = 0; k < K; ++k) {
                sum += A[(m * K + k) * strideA] * B[(k * N + n) * strideB];
            }
            C[(m * N + n) * strideC] = sum;
        }
    }
}

void testInt32Matmul(
    size_t M, size_t N, size_t K, 
    size_t strideA = 0, size_t strideB = 0, size_t strideC = 0,
    size_t repeatTimes = 16
) {
    
    using Integer = uint32_t;

    // allocate matrices
    Integer* hostBuffer = new Integer[M * K * strideA + K * N * strideB + M * N * strideC];
    Integer* A = hostBuffer;
    Integer* B = hostBuffer + M * K * strideA;
    Integer* C = hostBuffer + M * K * strideA + K * N * strideB;

    Timer timer;
    size_t tAllocate = timer.addTimer("Allocate");
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tCudaMatmul = timer.addTimer("CudaMatmul");
    size_t tCopyToHost = timer.addTimer("CopyToHost");
    size_t tFree = timer.addTimer("Free");
    size_t tReferenceMatmul = timer.addTimer("ReferenceMatmul");

    for (size_t rep = 0; rep < repeatTimes; rep++) {

        // fill matrices
        fillBytes((uint8_t*)A, M * K * sizeof(Integer));
        fillBytes((uint8_t*)B, K * N * sizeof(Integer));

        // create device memory and copy
        timer.tick(tAllocate);
        Integer* deviceBuffer = allocateDeviceArray<Integer>(M * K + K * N + M * N);
        timer.tock(tAllocate);
        timer.tick(tCopyToDevice);
        Integer* deviceA = deviceBuffer;
        Integer* deviceB = deviceBuffer + M * K;
        Integer* deviceC = deviceBuffer + M * K + K * N;
        copyToDeviceStrided((int32_t*)deviceA, (int32_t*)A, strideA, M * K);
        copyToDeviceStrided((int32_t*)deviceB, (int32_t*)B, strideB, K * N);
        timer.tock(tCopyToDevice);

        // perform matrix multiplication
        timer.tick(tCudaMatmul);
        deviceUint32Matmul(N, M, K, deviceB, deviceA, deviceC);
        timer.tock(tCudaMatmul);

        // copy back to host memory
        timer.tick(tCopyToHost);
        copyToHostStrided((int32_t*)C, (int32_t*)deviceC, strideC, M * N);
        timer.tock(tCopyToHost);
        // free device memory
        timer.tick(tFree);
        freeDeviceArray((int32_t*)deviceBuffer, M * K + K * N + M * N);
        timer.tock(tFree);

        // reference matmul
        timer.tick(tReferenceMatmul);
        referenceIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
        timer.tock(tReferenceMatmul);

    }

    timer.printAll();

    delete[] hostBuffer;

}


void testInt32AsDoubleMatmul(
    size_t M, size_t N, size_t K, 
    size_t strideA = 0, size_t strideB = 0, size_t strideC = 0,
    size_t repeatTimes = 16
) {
    
    using Integer = uint32_t;

    // allocate matrices
    Integer* hostBuffer = new Integer[M * K * strideA + K * N * strideB + M * N * strideC];
    Integer* A = hostBuffer;
    Integer* B = hostBuffer + M * K * strideA;
    Integer* C = hostBuffer + M * K * strideA + K * N * strideB;

    Timer timer;
    size_t tAllocate = timer.addTimer("Allocate");
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tDecompose = timer.addTimer("Decompose");
    size_t tCudaMatmul = timer.addTimer("CudaMatmul");
    size_t tCompose = timer.addTimer("Compose");
    size_t tCopyToHost = timer.addTimer("CopyToHost");
    size_t tFree = timer.addTimer("Free");
    size_t tReferenceMatmul = timer.addTimer("ReferenceMatmul");

    for (size_t rep = 0; rep < repeatTimes; rep++) {

        // fill matrices
        fillBytes((uint8_t*)A, M * K * sizeof(Integer));
        fillBytes((uint8_t*)B, K * N * sizeof(Integer));
        
        timer.tick(tAllocate);
        // create device memory and copy
        size_t sizeA = M * K;
        size_t sizeB = K * N;
        size_t sizeC = M * N;
        size_t total = sizeA + sizeB + sizeC;
        Integer* intBuffer = allocateDeviceArray<uint32_t>(total);
        double* doubleBuffer = allocateDeviceArray<double>(total * 2 + sizeC);
        timer.tock(tAllocate);
        timer.tick(tCopyToDevice);
        Integer* intA = intBuffer;
        Integer* intB = intBuffer + sizeA;
        Integer* intC = intBuffer + sizeA + sizeB;
        double* doubleA = doubleBuffer;
        double* doubleB = doubleBuffer + sizeA * 2;
        double* doubleC = doubleBuffer + (sizeA + sizeB) * 2;
        double* temp = doubleBuffer + total * 2;
        // copy to device memory
        copyToDeviceStrided(intA, A, strideA, sizeA);
        copyToDeviceStrided(intB, B, strideB, sizeB);
        timer.tock(tCopyToDevice);

        // decompose into double
        timer.tick(tDecompose);
        KERNEL_CALL(decomposeUint32, sizeA)(doubleA, intA, sizeA);
        KERNEL_CALL(decomposeUint32, sizeB)(doubleB, intB, sizeB);
        timer.tock(tDecompose);

        // perform matrix multiplication
        timer.tick(tCudaMatmul);
        // - lower 16 bits
        deviceDoubleMatmul(N, M, K, doubleB, doubleA, doubleC);
        // - higher 16 bits
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA, doubleC + sizeC);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, doubleC + sizeC, temp);
        // - carry
        deviceArrayCarry16(sizeC, doubleC + sizeC, doubleC);
        // - overflow
        deviceArrayReduce16Inplace(sizeC, doubleC + sizeC);
        timer.tock(tCudaMatmul);

        // compose back to int
        timer.tick(tCompose);
        KERNEL_CALL(composeUint32, sizeC)(intC, doubleC, sizeC);
        timer.tock(tCompose);

        // copy back to host memory
        timer.tick(tCopyToHost);
        copyToHostStrided(C, intC, strideC, sizeC);
        timer.tock(tCopyToHost);
        // free device memory
        timer.tick(tFree);
        freeDeviceArray(intBuffer, total);
        freeDeviceArray(doubleBuffer, total * 2 + sizeC);
        timer.tock(tFree);

        // reference matmul
        timer.tick(tReferenceMatmul);
        referenceIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
        timer.tock(tReferenceMatmul);
    }

    timer.printAll();

    delete[] hostBuffer;

}

void testInt64Matmul(
    size_t M, size_t N, size_t K, 
    size_t strideA = 0, size_t strideB = 0, size_t strideC = 0,
    size_t repeatTimes = 16
) {
    
    using Integer = uint64_t;

    // allocate matrices
    Integer* hostBuffer = new Integer[M * K * strideA + K * N * strideB + M * N * strideC];
    Integer* A = hostBuffer;
    Integer* B = hostBuffer + M * K * strideA;
    Integer* C = hostBuffer + M * K * strideA + K * N * strideB;

    Timer timer;
    size_t tAllocate = timer.addTimer("Allocate");
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tCudaMatmul = timer.addTimer("CudaMatmul");
    size_t tCopyToHost = timer.addTimer("CopyToHost");
    size_t tFree = timer.addTimer("Free");
    size_t tReferenceMatmul = timer.addTimer("ReferenceMatmul");

    for (size_t rep = 0; rep < repeatTimes; rep++) {
        // fill matrices
        fillBytes((uint8_t*)A, M * K * sizeof(Integer));
        fillBytes((uint8_t*)B, K * N * sizeof(Integer));

        timer.tick(tAllocate);
        // create device memory and copy
        Integer* deviceBuffer = allocateDeviceArray<Integer>(M * K + K * N + M * N);
        Integer* deviceA = deviceBuffer;
        Integer* deviceB = deviceBuffer + M * K;
        Integer* deviceC = deviceBuffer + M * K + K * N;
        timer.tock(tAllocate);
        timer.tick(tCopyToDevice);
        copyToDeviceStrided((int64_t*)deviceA, (int64_t*)A, strideA, M * K);
        copyToDeviceStrided((int64_t*)deviceB, (int64_t*)B, strideB, K * N);
        timer.tock(tCopyToDevice);

        // perform matrix multiplication
        timer.tick(tCudaMatmul);
        deviceUint64Matmul(N, M, K, deviceB, deviceA, deviceC);
        timer.tock(tCudaMatmul);

        // copy back to host memory
        timer.tick(tCopyToHost);
        copyToHostStrided((int64_t*)C, (int64_t*)deviceC, strideC, M * N);
        timer.tock(tCopyToHost);
        // free device memory
        timer.tick(tFree);
        freeDeviceArray((int64_t*)deviceBuffer, M * K + K * N + M * N);
        timer.tock(tFree);

        // reference matmul
        timer.tick(tReferenceMatmul);
        referenceIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
        timer.tock(tReferenceMatmul);
    }

    timer.printAll();

    delete[] hostBuffer;

}

void testInt64AsDoubleMatmul(
    size_t M, size_t N, size_t K, 
    size_t strideA = 0, size_t strideB = 0, size_t strideC = 0,
    size_t repeatTimes = 16
) {
    
    using Integer = uint64_t;

    // allocate matrices
    Integer* hostBuffer = new Integer[M * K * strideA + K * N * strideB + M * N * strideC];
    Integer* A = hostBuffer;
    Integer* B = hostBuffer + M * K * strideA;
    Integer* C = hostBuffer + M * K * strideA + K * N * strideB;

    Timer timer;
    size_t tAllocate = timer.addTimer("Allocate");
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tDecompose = timer.addTimer("Decompose");
    size_t tCudaMatmul = timer.addTimer("CudaMatmul");
    size_t tCompose = timer.addTimer("Compose");
    size_t tCopyToHost = timer.addTimer("CopyToHost");
    size_t tFree = timer.addTimer("Free");
    size_t tReferenceMatmul = timer.addTimer("ReferenceMatmul");

    for (size_t rep = 0; rep < repeatTimes; rep++) {

        // fill matrices
        fillBytes((uint8_t*)A, M * K * sizeof(Integer));
        fillBytes((uint8_t*)B, K * N * sizeof(Integer));

        timer.tick(tAllocate);
        // create device memory and copy
        size_t sizeA = M * K;
        size_t sizeB = K * N;
        size_t sizeC = M * N;
        size_t total = sizeA + sizeB + sizeC;
        Integer* intBuffer = allocateDeviceArray<uint64_t>(total);
        double* doubleBuffer = allocateDeviceArray<double>(total * 4 + sizeC);
        Integer* intA = intBuffer;
        Integer* intB = intBuffer + sizeA;
        Integer* intC = intBuffer + sizeA + sizeB;
        double* doubleA = doubleBuffer;
        double* doubleB = doubleBuffer + sizeA * 4;
        double* doubleC = doubleBuffer + (sizeA + sizeB) * 4;
        double* temp = doubleBuffer + total * 4;
        timer.tock(tAllocate);
        timer.tick(tCopyToDevice);
        // copy to device memory
        copyToDeviceStrided(intA, A, strideA, sizeA);
        copyToDeviceStrided(intB, B, strideB, sizeB);
        timer.tock(tCopyToDevice);

        // decompose into double
        timer.tick(tDecompose);
        KERNEL_CALL(decomposeUint64, sizeA)(doubleA, intA, sizeA);
        KERNEL_CALL(decomposeUint64, sizeB)(doubleB, intB, sizeB);
        timer.tock(tDecompose);

        // perform matrix multiplication
        timer.tick(tCudaMatmul);
        // - 0~16 bits
        double* targetC = doubleC;
        deviceDoubleMatmul(N, M, K, doubleB, doubleA, doubleC);
        // - 16~32 bits
        targetC = doubleC + sizeC;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA, targetC);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC);
        // - 32~64 bits
        targetC = doubleC + sizeC * 2;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC);
        // - 48~64 bits
        targetC = doubleC + sizeC * 3;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 2);
        // overflow
        deviceArrayReduce16Inplace(sizeC, targetC);
        timer.tock(tCudaMatmul);

        // compose back to int
        timer.tick(tCompose);
        KERNEL_CALL(composeUint64, sizeC)(intC, doubleC, sizeC);
        timer.tock(tCompose);

        // copy back to host memory
        timer.tick(tCopyToHost);
        copyToHostStrided(C, intC, strideC, sizeC);
        timer.tock(tCopyToHost);
        // free device memory
        timer.tick(tFree);
        freeDeviceArray(intBuffer, total);
        freeDeviceArray(doubleBuffer, total * 4 + sizeC);
        timer.tock(tFree);

        // reference matmul
        timer.tick(tReferenceMatmul);
        referenceIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
        timer.tock(tReferenceMatmul);
    }

    timer.printAll();

    delete[] hostBuffer;

}

void testInt128Matmul(
    size_t M, size_t N, size_t K, 
    size_t strideA = 0, size_t strideB = 0, size_t strideC = 0,
    size_t repeatTimes = 16
) {
    
    using Integer = __uint128_t;
    using int128_t = __int128_t;

    // allocate matrices
    Integer* hostBuffer = new Integer[M * K * strideA + K * N * strideB + M * N * strideC];
    Integer* A = hostBuffer;
    Integer* B = hostBuffer + M * K * strideA;
    Integer* C = hostBuffer + M * K * strideA + K * N * strideB;
    
    Timer timer;
    size_t tAllocate = timer.addTimer("Allocate");
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tCudaMatmul = timer.addTimer("CudaMatmul");
    size_t tCopyToHost = timer.addTimer("CopyToHost");
    size_t tFree = timer.addTimer("Free");
    size_t tReferenceMatmul = timer.addTimer("ReferenceMatmul");

    for (size_t rep = 0; rep < repeatTimes; rep++) {
        
        // fill matrices
        fillBytes((uint8_t*)A, M * K * sizeof(Integer));
        fillBytes((uint8_t*)B, K * N * sizeof(Integer));

        timer.tick(tAllocate);
        // create device memory and copy
        Integer* deviceBuffer = allocateDeviceArray<Integer>(M * K + K * N + M * N);
        Integer* deviceA = deviceBuffer;
        Integer* deviceB = deviceBuffer + M * K;
        Integer* deviceC = deviceBuffer + M * K + K * N;
        timer.tock(tAllocate);
        timer.tick(tCopyToDevice);
        copyToDeviceStrided((int128_t*)deviceA, (int128_t*)A, strideA, M * K);
        copyToDeviceStrided((int128_t*)deviceB, (int128_t*)B, strideB, K * N);
        timer.tock(tCopyToDevice);

        // perform matrix multiplication
        timer.tick(tCudaMatmul);
        deviceUint64x2Matmul(N, M, K, 
            reinterpret_cast<const uint64x2_t*>(deviceB), 
            reinterpret_cast<const uint64x2_t*>(deviceA), 
            reinterpret_cast<uint64x2_t*>(deviceC)
        );
        timer.tock(tCudaMatmul);

        // copy back to host memory
        timer.tick(tCopyToHost);
        copyToHostStrided((int128_t*)C, (int128_t*)deviceC, strideC, M * N);
        timer.tock(tCopyToHost);
        // free device memory
        timer.tick(tFree);
        freeDeviceArray((int128_t*)deviceBuffer, M * K + K * N + M * N);
        timer.tock(tFree);

        // reference matmul
        timer.tick(tReferenceMatmul);
        referenceIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
        timer.tock(tReferenceMatmul);
    }

    timer.printAll();

    delete[] hostBuffer;

}

void testInt128AsDoubleMatmul(
    size_t M, size_t N, size_t K, 
    size_t strideA = 0, size_t strideB = 0, size_t strideC = 0,
    size_t repeatTimes = 16
) {
    
    using Integer = uint128_t;

    // allocate matrices
    Integer* hostBuffer = new Integer[M * K * strideA + K * N * strideB + M * N * strideC];
    Integer* A = hostBuffer;
    Integer* B = hostBuffer + M * K * strideA;
    Integer* C = hostBuffer + M * K * strideA + K * N * strideB;

    Timer timer;
    size_t tAllocate = timer.addTimer("Allocate");
    size_t tCopyToDevice = timer.addTimer("CopyToDevice");
    size_t tDecompose = timer.addTimer("Decompose");
    size_t tCudaMatmul = timer.addTimer("CudaMatmul");
    size_t tCompose = timer.addTimer("Compose");
    size_t tCopyToHost = timer.addTimer("CopyToHost");
    size_t tFree = timer.addTimer("Free");
    size_t tReferenceMatmul = timer.addTimer("ReferenceMatmul");

    for (size_t rep = 0; rep < repeatTimes; rep++) {

        // fill matrices
        fillBytes((uint8_t*)A, M * K * sizeof(Integer));
        fillBytes((uint8_t*)B, K * N * sizeof(Integer));

        timer.tick(tAllocate);
        // create device memory and copy
        size_t sizeA = M * K;
        size_t sizeB = K * N;
        size_t sizeC = M * N;
        size_t total = sizeA + sizeB + sizeC;
        Integer* intBuffer = allocateDeviceArray<uint128_t>(total);
        double* doubleBuffer = allocateDeviceArray<double>(total * 8 + sizeC);
        Integer* intA = intBuffer;
        Integer* intB = intBuffer + sizeA;
        Integer* intC = intBuffer + sizeA + sizeB;
        double* doubleA = doubleBuffer;
        double* doubleB = doubleBuffer + sizeA * 8;
        double* doubleC = doubleBuffer + (sizeA + sizeB) * 8;
        double* temp = doubleBuffer + total * 8;
        timer.tock(tAllocate);
        timer.tick(tCopyToDevice);
        // copy to device memory
        copyToDeviceStrided(intA, A, strideA, sizeA);
        copyToDeviceStrided(intB, B, strideB, sizeB);
        timer.tock(tCopyToDevice);

        // decompose into double
        timer.tick(tDecompose);
        KERNEL_CALL(decomposeUint128, sizeA)(doubleA, intA, sizeA);
        KERNEL_CALL(decomposeUint128, sizeB)(doubleB, intB, sizeB);
        timer.tock(tDecompose);

        // perform matrix multiplication
        timer.tick(tCudaMatmul);
        // - 0~16 bits
        double* targetC = doubleC;
        deviceDoubleMatmul(N, M, K, doubleB, doubleA, doubleC);
        // - 16~32 bits
        targetC = doubleC + sizeC;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 0);
        // - 32~64 bits
        targetC = doubleC + sizeC * 2;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 1);
        // - 48~64 bits
        targetC = doubleC + sizeC * 3;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 2);
        // - 64~80 bits
        targetC = doubleC + sizeC * 4;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 3);
        // - 80~96 bits
        targetC = doubleC + sizeC * 5;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 5, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 5, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 4);
        // - 96~112 bits
        targetC = doubleC + sizeC * 6;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 6, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 5, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 5, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 6, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 5);
        // - 112~128 bits
        targetC = doubleC + sizeC * 7;
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 7, doubleA + sizeA * 0, targetC);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 6, doubleA + sizeA * 1, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 5, doubleA + sizeA * 2, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 4, doubleA + sizeA * 3, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 3, doubleA + sizeA * 4, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 2, doubleA + sizeA * 5, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 1, doubleA + sizeA * 6, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceDoubleMatmul(N, M, K, doubleB + sizeB * 0, doubleA + sizeA * 7, temp);
        deviceArrayAddInplace(sizeC, targetC, temp);
        deviceArrayCarry16(sizeC, targetC, doubleC + sizeC * 6);
        // - overflow
        deviceArrayReduce16Inplace(sizeC, targetC);
        timer.tock(tCudaMatmul);

        // compose back to int
        timer.tick(tCompose);
        KERNEL_CALL(composeUint128, sizeC)(intC, doubleC, sizeC);
        timer.tock(tCompose);

        // copy back to host memory
        timer.tick(tCopyToHost);
        copyToHostStrided(C, intC, strideC, sizeC);
        timer.tock(tCopyToHost);
        // free device memory
        timer.tick(tFree);
        freeDeviceArray(intBuffer, total);
        freeDeviceArray(doubleBuffer, total * 8 + sizeC);
        timer.tock(tFree);

        // reference matmul
        timer.tick(tReferenceMatmul);
        referenceIntMatmulStrided(M, N, K, A, B, C, strideA, strideB, strideC);
        timer.tock(tReferenceMatmul);
    }

    timer.printAll();

    delete[] hostBuffer;

}

// main func with args
// takes 4 or 7 integer arguments
// bitwidth, M, N, K, [strideA, strideB, strideC]

int main(int argc, char** argv) {

    if (argc != 5 && argc != 8) {
        std::cout << "Usage: ./test_int_gemm bitwidth M N K [strideA strideB strideC]" << std::endl;
        return 1;
    }

    char* bitwidth = argv[1];
    size_t M = atoi(argv[2]);
    size_t N = atoi(argv[3]);
    size_t K = atoi(argv[4]);
    size_t strideA = 0;
    size_t strideB = 0;
    size_t strideC = 0;
    if (argc == 8) {
        strideA = atoi(argv[5]);
        strideB = atoi(argv[6]);
        strideC = atoi(argv[7]);
    }

    std::cout <<
        "Settings" << std::endl <<
        "    bitwidth: " << bitwidth << std::endl <<
        "    M: " << M << std::endl <<
        "    N: " << N << std::endl <<
        "    K: " << K << std::endl;
    if (strideA != 0) std::cout << 
        "    strideA: " << strideA << std::endl;
    if (strideB != 0) std::cout << 
        "    strideB: " << strideB << std::endl;
    if (strideC != 0) std::cout << 
        "    strideC: " << strideC << std::endl;
    
    // do a dummy cudamalloc to initialize cuda context
    int32_t* dummy;
    cudaMalloc((void**)&dummy, 0);

    if (strideA == 0) strideA = 1;
    if (strideB == 0) strideB = 1;
    if (strideC == 0) strideC = 1;


    if (strcmp(bitwidth, "32") == 0) {
        testInt32Matmul(M, N, K, strideA, strideB, strideC);
    } else if (strcmp(bitwidth, "32d") == 0) {
        testInt32AsDoubleMatmul(M, N, K, strideA, strideB, strideC);
    } else if (strcmp(bitwidth, "64") == 0) {
        testInt64Matmul(M, N, K, strideA, strideB, strideC);
    } else if (strcmp(bitwidth, "64d") == 0) {
        testInt64AsDoubleMatmul(M, N, K, strideA, strideB, strideC);
    } else if (strcmp(bitwidth, "128") == 0) {
        testInt128Matmul(M, N, K, strideA, strideB, strideC);
    } else if (strcmp(bitwidth, "128d") == 0) {
        testInt128AsDoubleMatmul(M, N, K, strideA, strideB, strideC);
    } else {
        std::cout << "Bitwidth must be 32 or 64" << std::endl;
        return 1;
    }

    return 0;
}