#include <stdint.h>
#include <cstddef>
#include <cstdlib>

inline void randomInt32(int32_t* out, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        out[i] = rand();
    }
}

inline void randomInt64(int64_t* out, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        out[i] = (int64_t)(rand()) + (((int64_t)(rand())) << 32);
    }
}

inline __uint128_t randomUint128() {
    return
        (((__uint128_t)((rand()))) <<  0) +
        (((__uint128_t)((rand()))) << 32) + 
        (((__uint128_t)((rand()))) << 64) + 
        (((__uint128_t)((rand()))) << 96);
}

inline void randomUint128(__uint128_t* out, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        out[i] = randomUint128();
    }
}

inline void randomInt128(__int128_t* out, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        out[i] = (__int128_t)(randomUint128());
    }
}